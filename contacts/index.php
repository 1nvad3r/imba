<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Контактная информация");
?>
	<h2 class="h2 cta-contacts__title fade-in">Свяжитесь
		с&nbsp;нами</h2>
	<ul class="cta-contacts__contacts fade-in">
		<li class="cta-contacts__contact"><p class="h3 cta-contacts__contact-title">Телефон</p>
			<span class="h3 cta-contacts__contact-link"><?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/phone.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false); ?></span>
		</li>
		<li class="cta-contacts__contact"><p class="h3 cta-contacts__contact-title">Почта</p>
			<span class="h3 cta-contacts__contact-link"><?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/email.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false); ?></span>
		</li>
	</ul>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>