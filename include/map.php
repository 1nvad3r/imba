<section class="section maps-contacts" id="map">
	<div class="container maps-contacts__container"><h2 class="h2 maps-contacts__title fade-in">Где нас найти</h2>
		<div class="maps-tabs">
			<ul class="maps-tabs__list fade-in">
				<li class="maps-tabs__item">
					<button class="lead-title tab-name maps-tabs__link" type="button">Офисы</button>
				</li>
				<li class="maps-tabs__item">
					<button class="lead-title tab-name maps-tabs__link" type="button">Склады</button>
				</li>
			</ul>
			<section class="maps-tabs__section">
				<div class="maps-tabs__map fade-in">
					<div class="maps-tabs__map-frame" id="mapOffice"></div>
				</div>
				<ul class="maps-tabs__info fade-in">
					<li class="maps-tabs__address">
						<div class="maps-tabs__address-line"></div>
						<p class="lead-text maps-tabs__address-city">Москва</p>
						<p class="fact maps-tabs__address-text">адрес</p>
						<p class="text maps-tabs__address-location">117246, г. Москва, Научный проезд, 14А, стр.&nbsp;1,
							БЦ&nbsp;SMART-PARK</p></li>
					<li class="maps-tabs__address">
						<div class="maps-tabs__address-line"></div>
						<p class="lead-text maps-tabs__address-city">Санкт-Петербург</p>
						<p class="fact maps-tabs__address-text">адрес</p>
						<p class="text maps-tabs__address-location">191040, г. Санкт-Петербург, Лиговский проспект, 50&nbsp;к.&nbsp;12,
							офис&nbsp;23</p></li>
				</ul>
			</section>
			<section class="maps-tabs__section">
				<div class="maps-tabs__map fade-in">
					<div class="maps-tabs__map-frame" id="mapWarehouses"></div>
				</div>
				<ul class="maps-tabs__info fade-in">
					<li class="maps-tabs__address">
						<div class="maps-tabs__address-line"></div>
						<p class="lead-text maps-tabs__address-city">Склад ООО «ИЛС»</p>
						<p class="fact maps-tabs__address-text">адрес</p>
						<p class="text maps-tabs__address-location">МО, Подольский район, Краснопахорское с/п, л-во
							Малинское, квартал 56</p><a class="button button_style_arrow maps-tabs__btn"
														href="../files/ILS.pdf" target="_blank">Инструкция как
							добраться </a></li>
					<li class="maps-tabs__address">
						<div class="maps-tabs__address-line"></div>
						<p class="lead-text maps-tabs__address-city">Склад ООО «Деловая Лига»</p>
						<p class="fact maps-tabs__address-text">адрес</p>
						<p class="text maps-tabs__address-location">г. Москва, ул. Шоссейная 90с57<a
								class="button button_style_arrow maps-tabs__btn" href="../files/delovaya-liga.pdf"
								target="_blank">Инструкция как добраться </a></p></li>
				</ul>
			</section>
		</div>
	</div>
</section>