<ul class="about-info__facts fade-in">
	<li class="about-fact">
		<div class="number about-fact__number">700+</div>
		<div class="text about-fact__text">Проектов ежегодно</div>
	</li>
	<li class="about-fact">
		<div class="number about-fact__number">150+</div>
		<div class="text about-fact__text">Заказчиков нам доверяют</div>
	</li>
	<li class="about-fact">
		<div class="number about-fact__number">350+</div>
		<div class="text about-fact__text">Полученных сертификаций вендоров у&nbsp;сотрудников тех.&nbsp;департамента</div>
	</li>
	<li class="about-fact">
		<div class="number about-fact__number">40+</div>
		<div class="text about-fact__text">Квалифицированных инженеров и&nbsp;архитекторов</div>
	</li>
	<li class="about-fact">
		<div class="number about-fact__number">2 млрд.</div>
		<div class="text about-fact__text">Оборот за&nbsp;2022 год</div>
	</li>
	<li class="about-fact">
		<div class="number about-fact__number">500+</div>
		<div class="text about-fact__text">Количество лет совокупного опыта инженеров</div>
	</li>
</ul>