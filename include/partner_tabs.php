<section class="section tab-partners" id="partners">
	<div class="container tab-partners__container">
		<div class="tabs">
			<ul class="tab-partners__list">
				<li class="tab-partners__item tab-partners__item_active" data-index="0" role="tab">
					<button class="lead-title tab-name tab-partners__link" type="button">Наши заказчики</button>
				</li>
				<li class="tab-partners__item" data-index="1" role="tab">
					<button class="lead-title tab-name tab-partners__link" type="button">Наши партнёры</button>
				</li>
			</ul>
			<section class="tab-partners__section tab-partners__section_active" role="tab-content" data-index="0">
				<?php
					if($APPLICATION->GetCurPage(false) == "/")
						global $arrFilter;
						$arrFilter = ["PROPERTY_ON_MAIN" => "3"];
				?>
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "tab-clients", [
					"IBLOCK_TYPE" => "content",
					"IBLOCK_ID" => IBID_CLIENTS,
					"SET_TITLE" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_SHADOW" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => ["DETAIL_PICTURE"],
					"PROPERTY_CODE" => ["ON_MAIN"],
				],
					false
				); ?>
			</section>
			<section class="tab-partners__section" role="tab-content" data-index="1">
				<?
					if($APPLICATION->GetCurPage(false) == "/")
						$arrFilter = ["PROPERTY_ON_MAIN" => "2"];
				?>
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "tab-partners", [
					"IBLOCK_TYPE" => "content",
					"IBLOCK_ID" => IBID_PARTNERS,
					"SET_TITLE" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_SHADOW" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => ["DETAIL_PICTURE"],
					"PROPERTY_CODE" => ["ON_MAIN"],
					"NEWS_COUNT" => '8'
				],
					false
				); ?>
			</section>
		</div>
	</div>
</section>