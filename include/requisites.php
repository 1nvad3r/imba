<section class="section link-requisites"><a class="link-requisites__btn" href="/requisites/" title="Реквизиты">
		<div class="container link-requisites__container">
			<div class="link-requisites__content">
				<div class="link-requisites__icon-box"><img class="link-requisites__icon" src="/images/icon.svg"
															alt=""/><img class="link-requisites__icon_white"
																		 src="/images/icon-white.svg" alt=""/></div>
				<h2 class="h2 link-requisites__title">Реквизиты компании</h2></div>
			<div class="link-requisites__arrow-box"><img class="link-requisites__arrow" src="/images/Arrow-black.svg"
														 alt=""/><img
					class="link-requisites__arrow link-requisites__arrow_white" src="/images/Arrow-white.svg" alt=""/>
			</div>
		</div>
	</a></section>