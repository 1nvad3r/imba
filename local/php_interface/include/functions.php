<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main\Application,
		Bitrix\Main\Context,
		Bitrix\Main\Request,
		Bitrix\Main\Server;

	function d($var, $die = false)
	{
		echo '<pre>';
		print_r($var);
		echo '</pre>';
		!$die ?: exit;
	}

	function formatPhone($phone)
	{
		$format = substr($phone, 0, 1) . '' . substr($phone, 1, 1) . ' (' . substr($phone, 2, 3) . ') ' . substr($phone, 5, 3) . ' ' . substr($phone, 8, 4);
		return $format;
	}

