<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="licenses-partners__content">
	<?foreach ($arResult["ITEMS"] as $key => $item):?>
		<?
		if ($item == end($arResult["ITEMS"])) {
			$class = 'licenses-partners__content-two fade-in';
		}
		elseif ($key == 0){
			$class = 'licenses-partners__content-one';
		}
		else {
			$class = 'licenses-partners__content-two';
		}
		?>
	<div class="<?= $class ?>">
		<div class="red-line licenses-partners__lines"></div>
		<div class="licenses-partners__logo"><img src="<?= $item["PREVIEW_PICTURE"]["SRC"]?>" alt="">
			<h3 class="h3"><?= $item['NAME']?></h3></div>
	</div>
	<?endforeach;?>
</div>
