<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<ul class="services__cards">
	<? foreach ($arResult["ITEMS"] as $key => $item): ?>
		<li class="services-card">
			<img class="services-card__img" src="<?= CFile::GetPath($item["PROPERTIES"]["LIST_IMAGE"]['VALUE']); ?>"
				 alt="">
			<p class="lead-text services-card__title"><?= $item['NAME'] ?></p>
			<?= $item['PREVIEW_TEXT'] ?>
			<? if ($item["PROPERTIES"]["INFOTEXT_ONE"]['VALUE']): ?>
				<a class="button button_style_arrow services-card__btn" title="" href="<?= $item['DETAIL_PAGE_URL'] ?>">
					читать подробнее</a>
			<? endif; ?>
		</li>
	<? endforeach; ?>
</ul>