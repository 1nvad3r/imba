<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<div class="tab-clients">
	<? foreach ($arResult["ITEMS"] as $key => $item): ?>
		<div class="tab-clients__img-box logo-img-box <? ($key > 7) ? $class = 'logo_unvisible' : $class = ''; echo $class; ?>">
			<img src="<?= CFile::GetPath($item['PROPERTIES']['ICON_DARK']['VALUE']); ?>"
				 class="tab-clients__img-white logo-img-box__grey-light">
			<img src="<?= CFile::GetPath($item['PROPERTIES']['ICON_LIGHT']['VALUE']); ?>"
				 class="tab-clients__img-black logo-img-box__grey-dark">
			<img src="<?= CFile::GetPath($item['PROPERTIES']['ICON_COLOR_DARK']['VALUE']); ?>"
				 class="tab-clients__img-color logo-img-box__color-light">
			<img src="<?= CFile::GetPath($item['PROPERTIES']['ICON_COLOR_LIGHT']['VALUE']); ?>"
				 class="tab-clients__img-color-white logo-img-box__color-dark"></div>
	<? endforeach; ?>
</div>
<button class="button button_style_link tab-clients__btn" type="button">
	посмотреть все
</button>