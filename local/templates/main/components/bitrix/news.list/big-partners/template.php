<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>
<section class="section big-logo">
	<div class="container big-logo__container">
		<div class="big-logo__line fade-in"></div>
		<div class="big-logo__content-logo fade-in"></div>
	</div>
</section>
<section class="section small-logo">
	<div class="container small-logo__container">
		<div class="small-logo__line fade-in"></div>
		<div class="small-logo__content-logo fade-in"></div>
	</div>
</section>
<script type="text/javascript">
	(() => {
		"use strict";
		const l = {
				scroll() {
					document.body.style.overflow = "";
				},
				unscroll() {
					document.body.style.overflow = "hidden";
				},
			},
			o = function (o) {
				(function (l) {
					(g.name.textContent = l.name),
						(g.subtitle.textContent = l.subtitle),
						(g.text.textContent = l.text),
						document.body.classList.contains("light-mode") ? (g.img.src = l.imgColorWhite) : (g.img.src = l.imgColor),
						"" == l.subtitle ? (g.marker.style.display = "none") : (g.marker.style.display = "");
				})(o),
					g.popupLogo.classList.add("popup-logo_visible"),
					l.unscroll();
			},
			g = {
				popupLogo: document.querySelector(".popup-logo"),
				img: document.querySelector(".popup-logo__img"),
				name: document.querySelector(".popup-logo__name"),
				subtitle: document.querySelector(".popup-logo__tag-text"),
				text: document.querySelector(".popup-logo__text"),
				marker: document.querySelector(".popup-logo__tag-marker"),
				closeBtn: document.querySelector(".popup-logo__close"),
			};

		function e() {
			g.popupLogo.classList.remove("popup-logo_visible"), l.scroll();
		}

		function s(l) {
			const o = document.createElement("button");
			return o.classList.add(l), o;
		}

		function a(l, ...o) {
			const g = document.createElement("img");
			return (g.src = l), g.classList.add(...o), g;
		}

		function t(l, o) {
			const g = document.createElement("p");
			return (
				(g.textContent = l),
					o.forEach((l) => {
						g.classList.add(l);
					}),
					g
			);
		}

		function i(...l) {
			const o = document.createElement("div");
			return o.classList.add(...l), o;
		}

		g.closeBtn.addEventListener("click", e),
			g.popupLogo.addEventListener("click", (l) => {
				l.target.classList.contains("popup-logo") && e();
			});
		const m = document.querySelector(".big-logo__content-logo"),
			r = document.querySelector(".small-logo__content-logo");

		class n {
			constructor(l) {
				(this._popup = l),
					(this.openPopup = this.openPopup.bind(this)),
					(this.closePopup = this.closePopup.bind(this)),
					(this._setEscHandler = this._setEscHandler.bind(this)),
					(this.setEventListeners = this.setEventListeners.bind(this));
			}

			openPopup(o) {
				o && (this._subject = o), this._popup.classList.add("popup-form_visible"), (document.body.style.overflow = "hidden"), l.unscroll(), document.addEventListener("keydown", this._setEscHandler);
			}

			closePopup() {
				this._popup.classList.remove("popup-form_visible"), l.scroll(), document.removeEventListener("keydown", this._setEscHandler);
			}

			_setEscHandler(l) {
				"Escape" === l.key && this.closePopup();
			}

			setEventListeners() {
				this._popup.addEventListener("mousedown", (l) => {
					(l.target.classList.contains("popup-form_visible") || l.target.closest(".popup-form__close")) && this.closePopup();
				});
			}
		}

		class p extends n {
			constructor(l, o) {
				super(l),
					(this._form = l.querySelector(".form")),
					(this._onSubmit = o),
					(this._inputList = this._form.querySelectorAll(".form__item")),
					(this._popupButton = this._form.querySelector(".form__btn")),
					(this._inputValues = {}),
					(this._hiddenInput = this._form.querySelector(".form__fields_input-hidden"));
			}

			openPopup(l) {
				super.openPopup(l), (this._hiddenInput.value = this._subject), console.log(this._hiddenInput.value);
			}

			closePopup() {
				this._form.reset(), super.closePopup();
			}

			_getInputValues() {
				return (
					this._inputList.forEach((l) => {
						this._inputValues[l.name] = l.value;
					}),
						this._inputValues
				);
			}

			setEventListeners() {
				super.setEventListeners(),
					this._form.addEventListener("submit", (l) => {
						l.preventDefault();
					});
			}
		}

		const c = {WRAPPER: document.querySelector(".wrapper"), CANVAS: document.querySelector(".canvas")},
			h = 0.003,
			b = 20,
			u = 0,
			k = 0,
			v = b,
			d = {MIN: -180, MAX: 180},
			C = {MIN: -90, MAX: 90},
			w = "#202020",
			W = "#FBFBFB",
			x = "#A42E2C",
			_ = "#670E0E",
			B = "#511112",
			f = "#340D0F",
			y = {
				rotation: 0,
				time: 0,
				ctx: void 0,
				dots: [],
				background: W,
				KKK: 0,
				WIDTH: 0,
				HEIGHT: 0,
				colorFill: x,
				colorStroke: _
			};
		try {
			P(),
				requestAnimationFrame(P),
				(function () {
					for (let l = 0; l < 500; l++) y.dots.push(I());
				})(),
				(function () {
					const l = c.CANVAS.getContext("2d");
					l.fillRect(0, 0, c.CANVAS.width, c.CANVAS.height), (y.ctx = l);
				})(),
				A(),
				A();
		} catch (l) {
		}

		function E(l) {
			l ? ((y.background = W), (y.colorFill = B), (y.colorStroke = f)) : ((y.background = w), (y.colorFill = x), (y.colorStroke = _));
		}

		function S(l) {
			const o = b / l.z,
				g = y.WIDTH / 2,
				e = y.HEIGHT / 2,
				s = y.KKK * o * l.x + g,
				a = -y.KKK * o * l.y + e;
			(l.xDisplay = s), (l.yDisplay = a), y.ctx.beginPath(), y.ctx.ellipse(s, a, 2, 2, 0, 0, 2 * Math.PI), y.ctx.fill();
		}

		function L(l) {
			l.links.forEach((o) => {
				y.ctx.beginPath(), y.ctx.moveTo(l.xDisplay, l.yDisplay), y.ctx.lineTo(o.xDisplay, o.yDisplay), y.ctx.stroke();
			});
		}

		function A() {
			P(),
				y.time++,
				y.dots.forEach((l) => {
					(l.a = l.a + l.da),
						(l.b = l.b + l.db),
						(function (l) {
							(l.x = u + 1 * Math.sin(-y.rotation - y.time / 1e3 + l.a) * Math.cos(l.b)), (l.y = k + 1 * Math.sin(l.b)), (l.z = v - 1 * Math.cos(l.b) * Math.cos(-y.rotation - y.time / 1e3 + l.a));
						})(l);
				}),
				y.dots.sort((l, o) => o.z - l.z),
				y.dots.forEach((l) => {
					(l.links = []),
						y.dots.forEach((o) => {
							var g, e;
							(g = l), (e = o), Math.sqrt(Math.pow(g.x - e.x, 2) + Math.pow(g.y - e.y, 2) + Math.pow(g.z - e.z, 2)) < 0.15 && l.links.push(o);
						});
				}),
				(y.ctx.fillStyle = y.background),
				y.ctx.fillRect(0, 0, y.WIDTH, y.HEIGHT),
				(y.ctx.fillStyle = y.colorFill),
				(y.ctx.strokeStyle = y.colorStroke),
				y.dots.forEach(L),
				y.dots.forEach(S),
				requestAnimationFrame(A);
		}

		function I(...l) {
			return l.length > 0
				? {a: l[0], b: l[1], da: 0, db: 0, xDisplay: 0, yDisplay: 0, links: []}
				: {
					a: (d.MAX - d.MIN) * Math.random() + d.MIN,
					b: (C.MAX - C.MIN) * Math.random() + C.MIN,
					da: h * Math.random() - 0.0015,
					db: h * Math.random() - 0.0015,
					links: []
				};
		}

		function P() {
			const l = c.CANVAS.clientWidth,
				o = c.CANVAS.clientHeight,
				g = Math.min(l, o);
			(y.KKK = g / 3), (y.WIDTH = l), (y.HEIGHT = o), (c.CANVAS.width = l), (c.CANVAS.height = o);
		}

		const M = document.querySelectorAll(".js_toggle_input");

		function q() {
			document.body.classList.add("light-mode"), E(!0), console.log("светлая тема");
		}

		function T() {
			document.body.classList.remove("light-mode"), E(!1), console.log("тёмная тема");
		}

		[
			<? foreach ($arResult["ITEMS"] as $key => $item): ?>
				<? if ($item['PROPERTIES']['BASIC_PARTNER']['VALUE']): ?>
				{
					imgWhite: "<?= CFile::GetPath($item['PROPERTIES']['ICON_DARK']['VALUE'])?>",
					imgColor: "<?= CFile::GetPath($item['PROPERTIES']['ICON_COLOR_DARK']['VALUE'])?>",
					imgBlack: "<?= CFile::GetPath($item['PROPERTIES']['ICON_LIGHT']['VALUE'])?>",
					imgColorWhite: "<?= CFile::GetPath($item['PROPERTIES']['ICON_COLOR_LIGHT']['VALUE'])?>",
					subtitle: "<?= $item["TAGS"]?>",
					name: "<?= $item["NAME"] ?>",
					text: "<?= str_replace(PHP_EOL, ' ', strip_tags($item['PREVIEW_TEXT'])) ?>",
				},
				<? endif; ?>
			<? endforeach; ?>
		].forEach((l) => {
			const g = (function (l) {
				const g = s("big-logo__btn"),
					e = i("big-logo__img-box", "logo-img-box"),
					m = a(l.imgWhite, "big-logo__img-white", "logo-img-box__grey-light"),
					r = a(l.imgBlack, "big-logo__img-black", "logo-img-box__grey-dark"),
					n = a(l.imgColor, "big-logo__img-color", "logo-img-box__color-light"),
					p = a(l.imgColorWhite, "big-logo__img-color-white", "logo-img-box__color-dark"),
					c = i("big-logo__text-box");
				return (
					i("big-logo__marker"),
						t(l.subtitle, ["text-s", "big-logo__logo-text"]),
						e.append(m),
						e.append(r),
						e.append(n),
						e.append(p),
						g.append(e),
					"" != l.subtitle && g.append(c),
						g.addEventListener("click", (g) => {
							o(l);
						}),
						g
				);
			})(l);
			m.append(g);
		}),
			[
				<? foreach ($arResult["ITEMS"] as $key => $item): ?>
					<? if (!$item['PROPERTIES']['BASIC_PARTNER']['VALUE']): ?>
					{
						imgWhite: "<?= CFile::GetPath($item['PROPERTIES']['ICON_DARK']['VALUE'])?>",
						imgColor: "<?= CFile::GetPath($item['PROPERTIES']['ICON_COLOR_DARK']['VALUE'])?>",
						imgBlack: "<?= CFile::GetPath($item['PROPERTIES']['ICON_LIGHT']['VALUE'])?>",
						imgColorWhite: "<?= CFile::GetPath($item['PROPERTIES']['ICON_COLOR_LIGHT']['VALUE'])?>",
						subtitle: "<?= $item["TAGS"]?>",
						name: "<?= $item["NAME"] ?>",
						text: "<?= str_replace(PHP_EOL, ' ', strip_tags($item['PREVIEW_TEXT'])) ?>",
					},
					<? endif; ?>
				<? endforeach; ?>
			].forEach((l) => {
				const g = (function (l) {
					const g = s("small-logo__btn"),
						e = i("small-logo__img-box", "logo-img-box"),
						m = a(l.imgWhite, "small-logo__img-white", "logo-img-box__grey-light"),
						r = a(l.imgBlack, "small-logo__img-black", "logo-img-box__grey-dark"),
						n = a(l.imgColor, "small-logo__img-color", "logo-img-box__color-light"),
						p = a(l.imgColorWhite, "small-logo__img-color-white", "logo-img-box__color-dark"),
						c = i("small-logo__text-box");
					return (
						i("small-logo__marker"),
							t(l.subtitle, ["text-s", "small-logo__logo-text"]),
							e.append(m),
							e.append(r),
							e.append(n),
							e.append(p),
							g.append(e),
						"" != l.subtitle && g.append(c),
							g.addEventListener("click", (g) => {
								o(l);
							}),
							g
					);
				})(l);
				r.append(g);
			}),
			"dark" === localStorage.getItem("theme")
				? (M.forEach((l) => {
					l.checked = !1;
				}),
					T())
				: (M.forEach((l) => {
					l.checked = !0;
				}),
					q()),
			M.forEach((l) => {
				l.addEventListener("change", () => {
					l.checked
						? (M.forEach((l) => {
							l.checked = !0;
						}),
							localStorage.setItem("theme", "light"),
							q())
						: (M.forEach((l) => {
							l.checked = !1;
						}),
							localStorage.setItem("theme", "dark"),
							T());
				});
			}),
			(function () {
				try {
					const o = document.querySelector(".js_header_btn"),
						g = document.querySelector(".js_menu"),
						e = document.querySelector(".js_menu_overlay"),
						s = document.querySelector(".js_menu_exit");
					(g.style.display = ""),
						o.addEventListener("click", () => {
							e.classList.add("active"), g.classList.add("active-half"), s.classList.add("active"), l.unscroll();
						}),
						s.addEventListener("click", () => {
							a();
						}),
						e.addEventListener("click", () => {
							a();
						});
					const a = () => {
						e.classList.remove("active"), g.classList.remove("active-half", "active-full"), s.classList.remove("active"), l.scroll();
					};
				} catch (l) {
				}
			})(),
			(function () {
				try {
					const l = document.querySelector("#popupform"),
						o = new p(l, () => {
							o.closePopup();
						});
					o.setEventListeners(),
						document.querySelectorAll(".popup-form-open").forEach((l) => {
							l.addEventListener("click", (l) => {
								const g = l.target.dataset.subject;
								o.openPopup(g);
							});
						});
				} catch (l) {
					console.log(l);
				}
			})();
	})();
</script>