<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container small-logo__container">
	<div class="small-logo__line fade-in"></div>
	<div class="small-logo__content-logo fade-in">
		<?foreach ($arResult["ITEMS"] as $key => $item):?>
			<button class="small-logo__btn">
				<div class="small-logo__img-box logo-img-box"><img
							src="/images/partners/small-logo/white/small-logo1.png"
							class="small-logo__img-white logo-img-box__grey-light"><img
							src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"
							class="small-logo__img-black logo-img-box__grey-dark"><img
							src="<?= $item['DETAIL_PICTURE']['SRC'] ?>"
							class="small-logo__img-color logo-img-box__color-light"><img
							src="/images/partners/small-logo/color-white/small-logo1.png"
							class="small-logo__img-color-white logo-img-box__color-dark"></div>
				<div class="small-logo__text-box"></div>
			</button>
		<?endforeach;?>
	</div>
</div>