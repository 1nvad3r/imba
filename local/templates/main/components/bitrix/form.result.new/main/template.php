<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<form class="form js_form fade-in" name="form-contact">
<?//$arResult["FORM_NOTE"]?>
<?if ($arResult["isFormNote"] != "Y")
{
?>

<fieldset class="form__fields">
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{

		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
		{
			echo $arQuestion["HTML_CODE"];
		}
		else {
			switch ($arQuestion['CAPTION']){
				case 'Имя':
					$js_class = 'js_input_name';
					break;
				case 'Телефон':
					$js_class = 'js_input_phone';
					break;
				case 'Почта':
					$js_class = 'js_input_email';
					break;
				case 'Компания':
					$js_class = 'js_input_company';
					break;
				default:
					$js_class = '';
			}
	?>
		<label class="form__label">
			<? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'text'): ?>
				<input class="js_form_item form__item <?= $js_class; ?> form__fields_input" <?if ($arQuestion["CAPTION"]=='Почта'): ?>type="email"<?else:?>type="text"<?endif;?> name="form_text_<?= $arQuestion['STRUCTURE'][0]['FIELD_ID']?>" placeholder="<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?>*<?endif;?>" <?if ($arQuestion["REQUIRED"] == "Y"):?>required="required"<?endif;?>/>
			<? endif; ?>
			<? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'): ?>
				<textarea name="form_textarea_<?= $arQuestion['STRUCTURE'][0]['FIELD_ID']?>" placeholder="<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?>*<?endif;?>" rows="3" class="js_form_item form__item form__fields_input form__fields_comment"></textarea>
			<? endif; ?>
		</label>
	<?
		}
	} //endwhile
	?>
	<label class="form__label form__confirm checkbox">
		<input name="send_form" value="Y" class="js_form_item js_input_confirm" type="checkbox"/>
		<span>Согласен с&nbsp;<a href="/files/policy.pdf" target="_blank" rel="nofollow">политикой обработки персональных данных</a></span>
	</label>
	<button class="button button_style_red form__btn js_form_btn" onSubmit="handleSubmit()"><?=htmlspecialcharsbx(trim($arResult["arForm"]["BUTTON"]) == '' ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></button>
</fieldset>
<?
} //endif (isFormNote)
?></form>