<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if($arResult['PROPERTIES']['INFOTEXT_ONE']['VALUE']): ?>
<div class="wrapper black-background">
	<section class="section cover-security black-background">
		<div class="cover-security__img">
			<img class="cover-security__img_color" src="<?= CFile::GetPath($arResult["PROPERTIES"]["DETAIL_IMAGE_DARK"]['VALUE']); ?>" alt=""/>
			<img class="cover-security__img_black" src="<?= CFile::GetPath($arResult["PROPERTIES"]["DETAIL_IMAGE"]['VALUE']); ?>" alt=""/>
		</div>
		<div class="container cover-security__container">
			<div class="cover-security__head">
				<h1 class="h1 cover-security__title fade-in">
					<?= $arResult['PROPERTIES']['H1_TITLE']['~VALUE']['TEXT']?>
				</h1>
				<div class="red-line cover-security__lines cover-security__line fade-in"></div>
				<button class="button button_style_red cover-security__btn fade-in popup-form-open" type="button"
						data-subject="Заявка с обложки ИБ">Оставить заявку
				</button>
			</div>500
			<ul class="cover-security__cards">
				<?foreach ($arResult['PROPERTIES']['MAIN_POINTS']['VALUE'] as $point): ?>
					<li class="cover-security__card fade-in">
						<div class="cover-security__card-line"></div>
						<div class="fact cover-security__card-text"><?= $point ?></div>
					</li>
				<?endforeach;?>
			</ul>
		</div>
	</section>
	<section class="section services-security black-background">
		<div class="container services-security__container">
			<h2 class="h2 services-security__title fade-in"><?= $arResult['PROPERTIES']['H2_TITLE']['~VALUE']['TEXT'] ?></h2>
			<div class="services-security__info">
				<?= $arResult['PROPERTIES']['INFOTEXT_ONE']['~VALUE']['TEXT'] ?>
			</div>
		</div>
	</section>
</div>
<?else:?>
	<section class="section decisions">
		<div class="container decisions__container">
			<h2 class="h2 decisions__title fade-in">Страница не заполнена</h2>
		</div>
	</section>
<?endif;?>
<?if($arResult['PROPERTIES']['INFOTEXT_TWO']['VALUE']): ?>
<section class="section decisions">
	<div class="container decisions__container"><h2 class="h2 decisions__title fade-in"><?= $arResult['PROPERTIES']['H2_TITLE_TWO']['~VALUE']['TEXT'] ?></h2>
		<?= $arResult['PROPERTIES']['INFOTEXT_TWO']['~VALUE']['TEXT'] ?>
	</div>
</section>
<?endif;?>

<?if($arResult['PROPERTIES']['TEXT_THREE']['VALUE']): ?>
<section class="section solutions black-background">
	<?= $arResult['PROPERTIES']['TEXT_THREE']['~VALUE']['TEXT'] ?>
</section>
<?endif;?>

<?if($arResult['PROPERTIES']['TEXT_FOUR']['VALUE']): ?>
<section class="section import-security">
	<?= $arResult['PROPERTIES']['TEXT_FOUR']['~VALUE']['TEXT'] ?>
</section>
<?endif;?>

<?if($arResult['PROPERTIES']['TEXT_FIVE']['VALUE']): ?>
<section class="section support-security">
	<?= $arResult['PROPERTIES']['TEXT_FIVE']['~VALUE']['TEXT'] ?>
</section>
<?endif;?>

<?if($arResult['PROPERTIES']['TEXT_SIX']['VALUE']): ?>
<section class="section our-solutions black-background">
	<?= $arResult['PROPERTIES']['TEXT_SIX']['~VALUE']['TEXT'] ?>
</section>
<?endif;?>

<?if($arResult['PROPERTIES']['TEXT_SEVEN']['VALUE']): ?>
<section class="section cases black-background">
	<?= $arResult['PROPERTIES']['TEXT_SEVEN']['~VALUE']['TEXT'] ?>
</section>
<?endif;?>

<?if($arResult['PROPERTIES']['TEXT_EIGHT']['VALUE']): ?>
<section class="section about-security black-background">
	<?= $arResult['PROPERTIES']['TEXT_EIGHT']['~VALUE']['TEXT'] ?>
</section>
<?endif;?>