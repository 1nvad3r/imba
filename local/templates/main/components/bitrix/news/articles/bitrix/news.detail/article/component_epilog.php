<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	global $APPLICATION;
	
	if(!empty($arResult['PREVIEW_PICTURE']['SRC'])){
		$ogImageUrl = $arResult['PREVIEW_PICTURE']['SRC'];
		$APPLICATION->SetPageProperty("image", $ogImageUrl);
	}