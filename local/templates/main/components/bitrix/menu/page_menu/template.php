<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<section class="about">
    <ul class="about__list">
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <li class="about__item">
            <a class="about__link" href="<?=$arItem["LINK"]?>">
                <?=$arItem["TEXT"]?>
            </a>
        </li>
    <?endforeach?>
    </ul>
</section>
<?endif?>