<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<ul class="burger__list">
		<? foreach ($arResult as $arItem): ?>
			<li class="h3 burger__item"><a class="burger__link<? if ($arItem["SELECTED"]): ?> active<? endif; ?>"
										   href="<?= $arItem["LINK"] ?>" title=""><?= $arItem["TEXT"] ?></a></li>
		<? endforeach ?>
	</ul>
<? endif ?>