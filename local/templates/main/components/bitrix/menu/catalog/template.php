<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <button class="header__catalog-btn" type="button" onclick="javascript:location.href='/catalog/'">
                                <a href="/catalog/"><span>
                                    <svg width="22" height="16">
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#header-catalog-btn'?>"></use>
                                    </svg>
                                    Каталог
									</span></a>
    </button>
    <nav class="header__catalog-menu menu-catalog js_menu_catalog">
        <button class="menu-catalog__exit js_menu_catalog_exit" type="button">
            <svg width="18" height="18">
                <use xlink:href="<?=SITE_TEMPLATE_PATH.'/images/sprite.svg#exit'?>"></use>
            </svg>
        </button>
        <ul class="menu-catalog__list js_menu_catalog_list">
        <?foreach($arResult as $key => $arItem):?>
            <?if($arItem["DEPTH_LEVEL"] == 1):?>
                <li><a <?=($key==0)?'class="active"':''?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
            <?endif;?>
        <?endforeach?>
        </ul>
        <div class="menu-catalog__cnt">
            <?foreach($arResult as $key => $arItem):?>
                <?if($arItem["DEPTH_LEVEL"] == 1):?>
                    <ul class="menu-catalog__sublist js_menu_catalog_sublist <?=($key==0)?'active':''?>">
                        <?if(!empty($arItem["CHILDRENS"])):?>
                            <?foreach ($arItem["CHILDRENS"] as $element):?>
                                <li><a href="<?=$element["LINK"]?>"><?=$element["TEXT"]?></a></li>
                            <?endforeach;?>
                        <?endif;?>
                    </ul>
                <?endif;?>
            <?endforeach?>
        </div>
    </nav>
<?endif?>