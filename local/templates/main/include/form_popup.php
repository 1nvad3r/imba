<form class="form js_form fade-in" name="form-popup">
	<fieldset class="form__fields"><label class="form__label"><input
					class="js_form_item form__item form__fields_input form__fields_input-hidden"
					type="hidden"
					name="Тема"></label><label class="form__label"><input
					class="js_form_item form__item js_input_name form__fields_input" type="text"
					name="first_name"
					placeholder="Имя*" required></label><label class="form__label"><input
					class="js_form_item form__item js_input_phone form__fields_input" type="text"
					name="phone"
					placeholder="Телефон*" required></label><label class="form__label"><input
					class="js_form_item form__item js_input_email form__fields_input" type="text"
					name="email"
					placeholder="Почта*" required></label><label class="form__label"><input
					class="js_form_item form__item js_input_company form__fields_input" type="text"
					name="company"
					placeholder="Компания*" required></label><label class="form__label"><textarea
					name="comment"
					placeholder="Ваши комментарии"
					rows="3"
					class="js_form_item form__item form__fields_input form__fields_comment"></textarea></label><label
				class="form__label form__confirm checkbox"><input
					class="js_form_item form__item js_input_confirm"
					type="checkbox" name="send_form" value="Y"><span>Согласен с&nbsp;<a
						href="/files/policy.pdf" target="_blank"
						rel="nofollow">политикой обработки персональных данных</a></span></label></fieldset>
	<button class="button button_style_red form__btn js_form_btn" onSubmit="handleSubmit()">оставить
		заявку
	</button>
</form>