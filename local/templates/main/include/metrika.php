<!-- Yandex.Metrika counter -->
<script>(function (m, e, t, r, i, k, a) {
		m[i] = m[i] || function () {
			(m[i].a = m[i].a || []).push(arguments)
		};
		m[i].l = 1 * new Date();
		k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
	})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(60670951, "init", {
		clickmap: true,
		trackLinks: true,
		accurateTrackBounce: true
	});</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/60670951" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript><!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-STTGKXB4CR"></script>
<script>window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}

	gtag('js', new Date());

	gtag('config', 'G-STTGKXB4CR');</script>
<script src="https://captcha-api.yandex.ru/captcha.js?render=onload&amp;onload=onloadFunction" defer></script>
<script>function onloadFunction() {
		if (!window.smartCaptcha) {
			return;
		}

		window.smartCaptcha.render('captcha-container', {
			sitekey: 'ysc1_7bIPH9RZyBaJB8W96gAmzcizrfzw8LJ3u7203Uwjbe2dd728',
			invisible: true, // Сделать капчу невидимой
			callback: callback,
			hideShield: true,
		});
	}

	function callback(token) {
		console.log(token);
	}</script>