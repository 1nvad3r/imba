// (()=>{"use strict";const e="fade-in",t="fade-in_visible",s=new IntersectionObserver((function(e){e.forEach((e=>{e.isIntersecting&&function(e){e.target.classList.add(t),setTimeout(n.bind(this,e.target),1e3)}(e)}))}),{root:null,rootMargin:"-20px 0px",threshold:0});function n(n){s.unobserve(n),n.classList.remove(e,t)}const a=document.querySelectorAll(".feedback"),o=(e,t)=>{t.classList.add("error"),t.classList.contains("error")&&t.closest("label").querySelector("span.error")?(t.closest("label").querySelector("span.error").remove(),t.closest("label").insertAdjacentHTML("beforeend",`<span class="error">${e}</span>`)):t.classList.contains("error")&&t.closest("label").insertAdjacentHTML("beforeend",`<span class="error">${e}</span>`)},r=e=>{e.classList.remove("error"),e.closest("label").querySelector("span.error")&&e.closest("label").querySelector("span.error").remove()},c=/([\w\s]*?[0-9][\w\s]*?){11,}/,l=/([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+/i;function i(e){e.value=e.value.trim(),e.classList.contains("js_input_name")&&""==e.value?o("Заполните, пожалуйста, поле",e):e.classList.contains("js_input_name")&&r(e),e.classList.contains("js_input_phone")&&""==e.value?o("Заполните, пожалуйста, поле",e):e.classList.contains("js_input_phone")&&-1!==e.value.search(c)?r(e):e.classList.contains("js_input_phone")&&o("Укажите, пожалуйста, корректный телефон",e),e.classList.contains("js_input_email")&&""==e.value?o("Заполните, пожалуйста, поле",e):e.classList.contains("js_input_email")&&-1!==e.value.search(l)?r(e):e.classList.contains("js_input_email")&&o("Укажите, пожалуйста, корректный e-mail",e),e.classList.contains("js_input_company")&&""==e.value?o("Заполните, пожалуйста, поле",e):e.classList.contains("js_input_company")&&r(e),e.classList.contains("js_input_confirm")&&e.checked?r(e):e.classList.contains("js_input_confirm")&&o("Поставьте, пожалуйста, галочку",e)}try{document.querySelectorAll(".fade-in").forEach((e=>{s.observe(e)})),document.querySelectorAll('a[href^="#"]').forEach((e=>{e.addEventListener("click",(function(e){e.preventDefault();let t=this.getAttribute("href").substring(1);const s=document.getElementById(t),n=document.querySelector(".header").offsetHeight,a=s.getBoundingClientRect().top-n;window.scrollBy({top:a,behavior:"smooth"}),closeMenu()}))})),document.querySelectorAll(".js_form").forEach((e=>{e.querySelector("button").addEventListener("click",(t=>{t.preventDefault();const s=e.querySelectorAll(".js_form_item"),n=e.querySelector(".js_form_btn");if(s.forEach((t=>{i(t),t.addEventListener("input",(()=>{i(t),e.querySelector(".error")||(n.disabled=!1)}))})),e.querySelector(".error"))console.log("no-validate"),n.disabled=!0;else{console.log("validate"),n.classList.add("btn-animate");let t=new FormData(e),s=new XMLHttpRequest;s.onreadystatechange=function(){4===s.readyState&&200===s.status&&(console.log("Отправлено"),document.querySelector(".popup-form").classList.remove("popup-form_visible"),document.querySelector(".js_feedback_success").classList.add("active"),n.classList.remove("btn-animate"))},s.open("POST","mail.php",!0),s.send(t),e.reset()}}))})),function(){a.forEach((t=>{t.querySelectorAll(".js_feedback_exit").forEach((s=>{s.addEventListener("click",(()=>{e(t)}))})),t.addEventListener("click",(s=>{s.target==t&&e(t)}))}));const e=e=>{e.classList.remove("active")}}()}catch(e){}})();
(() => {
	"use strict";
	const e = "fade-in",
		t = "fade-in_visible",
		s = new IntersectionObserver(
			function (e) {
				e.forEach((e) => {
					e.isIntersecting &&
					(function (e) {
						e.target.classList.add(t), setTimeout(n.bind(this, e.target), 1e3);
					})(e);
				});
			},
			{ root: null, rootMargin: "-20px 0px", threshold: 0 }
		);
	function n(n) {
		s.unobserve(n), n.classList.remove(e, t);
	}
	const a = document.querySelectorAll(".feedback"),
		o = (e, t) => {
			t.classList.add("error"),
				t.classList.contains("error") && t.closest("label").querySelector("span.error")
					? (t.closest("label").querySelector("span.error").remove(), t.closest("label").insertAdjacentHTML("beforeend", `<span class="error">${e}</span>`))
					: t.classList.contains("error") && t.closest("label").insertAdjacentHTML("beforeend", `<span class="error">${e}</span>`);
		},
		r = (e) => {
			e.classList.remove("error"), e.closest("label").querySelector("span.error") && e.closest("label").querySelector("span.error").remove();
		},
		c = /([\w\s]*?[0-9][\w\s]*?){11,}/,
		l = /([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+/i;
	function i(e) {
		(e.value = e.value.trim()),
			e.classList.contains("js_input_name") && "" == e.value ? o("Заполните, пожалуйста, поле", e) : e.classList.contains("js_input_name") && r(e),
			e.classList.contains("js_input_phone") && "" == e.value
				? o("Заполните, пожалуйста, поле", e)
				: e.classList.contains("js_input_phone") && -1 !== e.value.search(c)
					? r(e)
					: e.classList.contains("js_input_phone") && o("Укажите, пожалуйста, корректный телефон", e),
			e.classList.contains("js_input_email") && "" == e.value
				? o("Заполните, пожалуйста, поле", e)
				: e.classList.contains("js_input_email") && -1 !== e.value.search(l)
					? r(e)
					: e.classList.contains("js_input_email") && o("Укажите, пожалуйста, корректный e-mail", e),
			e.classList.contains("js_input_company") && "" == e.value ? o("Заполните, пожалуйста, поле", e) : e.classList.contains("js_input_company") && r(e),
			e.classList.contains("js_input_confirm") && e.checked ? r(e) : e.classList.contains("js_input_confirm") && o("Поставьте, пожалуйста, галочку", e);
	}
	try {
		document.querySelectorAll(".fade-in").forEach((e) => {
			s.observe(e);
		}),
			document.querySelectorAll('a[href^="#"]').forEach((e) => {
				e.addEventListener("click", function (e) {
					e.preventDefault();
					let t = this.getAttribute("href").substring(1);
					const s = document.getElementById(t),
						n = document.querySelector(".header").offsetHeight,
						a = s.getBoundingClientRect().top - n;
					window.scrollBy({ top: a, behavior: "smooth" }), closeMenu();
				});
			}),
			document.querySelectorAll(".js_form").forEach((e) => {
				e.querySelector("button").addEventListener("click", (t) => {
					t.preventDefault();
					const s = e.querySelectorAll(".js_form_item"),
						n = e.querySelector(".js_form_btn");
					if (
						(s.forEach((t) => {
							i(t),
								t.addEventListener("input", () => {
									i(t), e.querySelector(".error") || (n.disabled = !1);
								});
						}),
							e.querySelector(".error"))
					)
						console.log("no-validate"), (n.disabled = !0);
					else {
						console.log("validate"), n.classList.add("btn-animate");
						let t = new FormData(e),
							s = new XMLHttpRequest();
						(s.onreadystatechange = function () {
							4 === s.readyState &&
							200 === s.status &&
							(console.log("Отправлено"),
								document.querySelector(".popup-form").classList.remove("popup-form_visible"),
								document.querySelector(".js_feedback_success").classList.add("active"),
								n.classList.remove("btn-animate"));
						}),
							s.open("POST", "mail.php", !0),
							s.send(t),
							e.reset();
					}
				});
			}),
			(function () {
				a.forEach((t) => {
					t.querySelectorAll(".js_feedback_exit").forEach((s) => {
						s.addEventListener("click", () => {
							e(t);
						});
					}),
						t.addEventListener("click", (s) => {
							s.target == t && e(t);
						});
				});
				const e = (e) => {
					e.classList.remove("active");
				};
			})();
	} catch (e) {}
})();