// (()=>{"use strict";const o={scroll(){document.body.style.overflow=""},unscroll(){document.body.style.overflow="hidden"}};class e{constructor(o){this._popup=o,this.openPopup=this.openPopup.bind(this),this.closePopup=this.closePopup.bind(this),this._setEscHandler=this._setEscHandler.bind(this),this.setEventListeners=this.setEventListeners.bind(this)}openPopup(e){e&&(this._subject=e),this._popup.classList.add("popup-form_visible"),document.body.style.overflow="hidden",o.unscroll(),document.addEventListener("keydown",this._setEscHandler)}closePopup(){this._popup.classList.remove("popup-form_visible"),o.scroll(),document.removeEventListener("keydown",this._setEscHandler)}_setEscHandler(o){"Escape"===o.key&&this.closePopup()}setEventListeners(){this._popup.addEventListener("mousedown",(o=>{(o.target.classList.contains("popup-form_visible")||o.target.closest(".popup-form__close"))&&this.closePopup()}))}}class t extends e{constructor(o,e){super(o),this._form=o.querySelector(".form"),this._onSubmit=e,this._inputList=this._form.querySelectorAll(".form__item"),this._popupButton=this._form.querySelector(".form__btn"),this._inputValues={},this._hiddenInput=this._form.querySelector(".form__fields_input-hidden")}openPopup(o){super.openPopup(o),this._hiddenInput.value=this._subject,console.log(this._hiddenInput.value)}closePopup(){this._form.reset(),super.closePopup()}_getInputValues(){return this._inputList.forEach((o=>{this._inputValues[o.name]=o.value})),this._inputValues}setEventListeners(){super.setEventListeners(),this._form.addEventListener("submit",(o=>{o.preventDefault()}))}}const i={WRAPPER:document.querySelector(".wrapper"),CANVAS:document.querySelector(".canvas")},g=.003,s=20,l=0,a=0,r=s,n={MIN:-180,MAX:180},c={MIN:-90,MAX:90},m="#202020",h="#FBFBFB",p="#A42E2C",b="#670E0E",_="#511112",u="#340D0F",d={rotation:0,time:0,ctx:void 0,dots:[],background:h,KKK:0,WIDTH:0,HEIGHT:0,colorFill:p,colorStroke:b};try{T(),requestAnimationFrame(T),function(){for(let o=0;o<500;o++)d.dots.push(W())}(),function(){const o=i.CANVAS.getContext("2d");o.fillRect(0,0,i.CANVAS.width,i.CANVAS.height),d.ctx=o}(),w(),w()}catch(o){}function v(o){o?(d.background=h,d.colorFill=_,d.colorStroke=u):(d.background=m,d.colorFill=p,d.colorStroke=b)}function k(o){const e=s/o.z,t=d.WIDTH/2,i=d.HEIGHT/2,g=d.KKK*e*o.x+t,l=-d.KKK*e*o.y+i;o.xDisplay=g,o.yDisplay=l,d.ctx.beginPath(),d.ctx.ellipse(g,l,2,2,0,0,2*Math.PI),d.ctx.fill()}function C(o){o.links.forEach((e=>{d.ctx.beginPath(),d.ctx.moveTo(o.xDisplay,o.yDisplay),d.ctx.lineTo(e.xDisplay,e.yDisplay),d.ctx.stroke()}))}function w(){T(),d.time++,d.dots.forEach((o=>{o.a=o.a+o.da,o.b=o.b+o.db,function(o){o.x=l+1*Math.sin(-d.rotation-d.time/1e3+o.a)*Math.cos(o.b),o.y=a+1*Math.sin(o.b),o.z=r-1*Math.cos(o.b)*Math.cos(-d.rotation-d.time/1e3+o.a)}(o)})),d.dots.sort(((o,e)=>e.z-o.z)),d.dots.forEach((o=>{o.links=[],d.dots.forEach((e=>{var t,i;t=o,i=e,Math.sqrt(Math.pow(t.x-i.x,2)+Math.pow(t.y-i.y,2)+Math.pow(t.z-i.z,2))<.15&&o.links.push(e)}))})),d.ctx.fillStyle=d.background,d.ctx.fillRect(0,0,d.WIDTH,d.HEIGHT),d.ctx.fillStyle=d.colorFill,d.ctx.strokeStyle=d.colorStroke,d.dots.forEach(C),d.dots.forEach(k),requestAnimationFrame(w)}function W(...o){return o.length>0?{a:o[0],b:o[1],da:0,db:0,xDisplay:0,yDisplay:0,links:[]}:{a:(n.MAX-n.MIN)*Math.random()+n.MIN,b:(c.MAX-c.MIN)*Math.random()+c.MIN,da:g*Math.random()-.0015,db:g*Math.random()-.0015,links:[]}}function T(){const o=i.CANVAS.clientWidth,e=i.CANVAS.clientHeight,t=Math.min(o,e);d.KKK=t/3,d.WIDTH=o,d.HEIGHT=e,i.CANVAS.width=o,i.CANVAS.height=e}const f=document.querySelectorAll(".js_toggle_input");function y(){document.body.classList.add("light-mode"),v(!0),console.log("светлая тема")}function E(){document.body.classList.remove("light-mode"),v(!1),console.log("тёмная тема")}const A=[{imgWhite:"/images/clients/white/logo1.svg",imgColor:"/images/clients/color/logo1.svg",imgBlack:"/images/clients/black/logo1.svg",imgColorWhite:"/images/clients/color-white/logo1.svg"},{imgWhite:"/images/clients/white/logo2.svg",imgColor:"/images/clients/color/logo2.svg",imgBlack:"/images/clients/black/logo2.svg",imgColorWhite:"/images/clients/color-white/logo2.svg"},{imgWhite:"/images/clients/white/logo3.svg",imgColor:"/images/clients/color/logo3.svg",imgBlack:"/images/clients/black/logo3.svg",imgColorWhite:"/images/clients/color-white/logo3.svg"},{imgWhite:"/images/clients/white/logo4.svg",imgColor:"/images/clients/color/logo4.svg",imgBlack:"/images/clients/black/logo4.svg",imgColorWhite:"/images/clients/color-white/logo4.svg"},{imgWhite:"/images/clients/white/logo5.png",imgColor:"/images/clients/color/logo5.png",imgBlack:"/images/clients/black/logo5.png",imgColorWhite:"/images/clients/color-white/logo5.png"},{imgWhite:"/images/clients/white/logo6.svg",imgColor:"/images/clients/color/logo6.svg",imgBlack:"/images/clients/black/logo6.svg",imgColorWhite:"/images/clients/color-white/logo6.svg"},{imgWhite:"/images/clients/white/logo7.svg",imgColor:"/images/clients/color/logo7.svg",imgBlack:"/images/clients/black/logo7.svg",imgColorWhite:"/images/clients/color-white/logo7.svg"},{imgWhite:"/images/clients/white/logo8.png",imgColor:"/images/clients/color/logo8.png",imgBlack:"/images/clients/black/logo8.png",imgColorWhite:"/images/clients/color-white/logo8.png"},{imgWhite:"/images/clients/white/logo9.png",imgColor:"/images/clients/color/logo9.png",imgBlack:"/images/clients/black/logo9.png",imgColorWhite:"/images/clients/color-white/logo9.png"},{imgWhite:"/images/clients/white/logo10.png",imgColor:"/images/clients/color/logo10.png",imgBlack:"/images/clients/black/logo10.png",imgColorWhite:"/images/clients/color-white/logo10.png"},{imgWhite:"/images/clients/white/logo11.png",imgColor:"/images/clients/color/logo11.png",imgBlack:"/images/clients/black/logo11.png",imgColorWhite:"/images/clients/color-white/logo11.png"},{imgWhite:"/images/clients/white/logo12.png",imgColor:"/images/clients/color/logo12.png",imgBlack:"/images/clients/black/logo12.png",imgColorWhite:"/images/clients/color-white/logo12.png"},{imgWhite:"/images/clients/white/logo13.png",imgColor:"/images/clients/color/logo13.png",imgBlack:"/images/clients/black/logo13.png",imgColorWhite:"/images/clients/color-white/logo13.png"},{imgWhite:"/images/clients/white/logo14.png",imgColor:"/images/clients/color/logo14.png",imgBlack:"/images/clients/black/logo14.png",imgColorWhite:"/images/clients/color-white/logo14.png"},{imgWhite:"/images/clients/white/logo15.png",imgColor:"/images/clients/color/logo15.png",imgBlack:"/images/clients/black/logo15.png",imgColorWhite:"/images/clients/color-white/logo15.png"},{imgWhite:"/images/clients/white/logo16.png",imgColor:"/images/clients/color/logo16.png",imgBlack:"/images/clients/black/logo16.png",imgColorWhite:"/images/clients/color-white/logo16.png"},{imgWhite:"/images/clients/white/logo17.png",imgColor:"/images/clients/color/logo17.png",imgBlack:"/images/clients/black/logo17.png",imgColorWhite:"/images/clients/color-white/logo17.png"}];function B(o,...e){const t=document.createElement("img");return t.src=o,t.classList.add(...e),t}function x(...o){const e=document.createElement("div");return e.classList.add(...o),e}const S=document.querySelector(".tab-clients");const L=[{imgWhite:"/images/partners/big-logo/white/logo1.png",imgColor:"/images/partners/big-logo/color/logo1.png",imgBlack:"/images/partners/big-logo/black/logo1.png",imgColorWhite:"/images/partners/big-logo/color-white/logo1.png",subtitle:"",name:"Код безопасности",text:"Российский разработчик программных и аппаратных средств, обеспечивающих полную защиту ИТ-инфраструктуры: конечных станций и серверов, периметра сети, современных виртуальных инфраструктур и мобильных устройств."},{imgWhite:"/images/partners/big-logo/white/logo2.svg",imgColor:"/images/partners/big-logo/color/logo2.svg",imgBlack:"/images/partners/big-logo/black/logo2.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo2.svg",subtitle:"Авторизованный партнер",name:"Positive technologies",text:"Российский разработчик решений для информационной безопасности."},{imgWhite:"/images/partners/big-logo/white/logo3.png",imgColor:"/images/partners/big-logo/color/logo3.png",imgBlack:"/images/partners/big-logo/black/logo3.png",imgColorWhite:"/images/partners/big-logo/color-white/logo3.png",subtitle:"Registered Partner",name:"Лаборатория Касперского",text:"Российский разработчик и производитель систем защиты от компьютерных вирусов, спама, хакерских атак и прочих киберугроз."},{imgWhite:"/images/partners/big-logo/white/logo4.png",imgColor:"/images/partners/big-logo/color/logo4.png",imgBlack:"/images/partners/big-logo/black/logo4.png",imgColorWhite:"/images/partners/big-logo/color-white/logo4.png",subtitle:"Partner",name:"Check Point",text:"Израильский разработчик решений по кибербезопасности."},{imgWhite:"/images/partners/big-logo/white/logo5.svg",imgColor:"/images/partners/big-logo/color/logo5.svg",imgBlack:"/images/partners/big-logo/black/logo5.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo5.svg",subtitle:"Bronze",name:"UserGate",text:"Российский разработчик программного обеспечения и микроэлектроники в сфере информационной безопасности."},{imgWhite:"/images/partners/big-logo/white/logo6.png",imgColor:"/images/partners/big-logo/color/logo6.png",imgBlack:"/images/partners/big-logo/black/logo6.png",imgColorWhite:"/images/partners/big-logo/color-white/logo6.png",subtitle:"",name:"Ruseim",text:"Российский производитель, занимающийся созданием решений в области мониторинга и управления событиями информационной безопасности на основе анализа данных в реальном времени."},{imgWhite:"/images/partners/big-logo/white/logo7.svg",imgColor:"/images/partners/big-logo/color/logo7.svg",imgBlack:"/images/partners/big-logo/black/logo7.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo7.svg",subtitle:"",name:"Aquarius",text:"Российский производитель компьютерной техники (серверы, системы хранения данных, клиентские устройства, отраслевые и специализированные IT‑решения и программно-аппаратные комплексы."},{imgWhite:"/images/partners/big-logo/white/logo8.png",imgColor:"/images/partners/big-logo/color/logo8.png",imgBlack:"/images/partners/big-logo/black/logo8.png",imgColorWhite:"/images/partners/big-logo/color-white/logo8.png",subtitle:"Бизнес-партнер",name:"Utinet",text:"Российский роизводитель серверов, систем хранения данных и программного обеспечения."},{imgWhite:"/images/partners/big-logo/white/logo9.png",imgColor:"/images/partners/big-logo/color/logo9.png",imgBlack:"/images/partners/big-logo/black/logo9.png",imgColorWhite:"/images/partners/big-logo/color-white/logo9.png",subtitle:"Авторизованный партнер",name:"Аэродиск",text:"Российский производитель инновационных решений в области хранения данных и виртуализации (системы хранения данных, гиперконвергентная система и программное обеспечение)."},{imgWhite:"/images/partners/big-logo/white/logo10.png",imgColor:"/images/partners/big-logo/color/logo10.png",imgBlack:"/images/partners/big-logo/black/logo10.png",imgColorWhite:"/images/partners/big-logo/color-white/logo10.png",subtitle:"Авторизованный партнер",name:"Звезда",text:"Российский разработчик и производитель вычислительного радиоэлектронного оборудования и ПО (вычислительные серверы, программно-определяемые системы хранения данных, автоматизированные рабочие места и тонкие клиенты). телекоммуникационное оборудование)"},{imgWhite:"/images/partners/big-logo/white/logo11.svg",imgColor:"/images/partners/big-logo/color/logo11.svg",imgBlack:"/images/partners/big-logo/black/logo11.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo11.svg",subtitle:"",name:"Eltex",text:"Российский разработчик и производитель телекоммуникационного и сетевого оборудования, промышленных контроллеров для IoT."},{imgWhite:"/images/partners/big-logo/white/logo12.png",imgColor:"/images/partners/big-logo/color/logo12.png",imgBlack:"/images/partners/big-logo/black/logo12.png",imgColorWhite:"/images/partners/big-logo/color-white/logo12.png",subtitle:"Авторизованный партнер",name:"Р7-Офис",text:"Российский разработчик офисного программного обеспечения."},{imgWhite:"/images/partners/big-logo/white/logo13.svg",imgColor:"/images/partners/big-logo/color/logo13.svg",imgBlack:"/images/partners/big-logo/black/logo13.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo13.svg",subtitle:"Авторизованный партнер",name:"Мой офис",text:"Российский разработчик безопасных офисных решений для общения и совместной работы с документами."},{imgWhite:"/images/partners/big-logo/white/logo14.svg",imgColor:"/images/partners/big-logo/color/logo14.svg",imgBlack:"/images/partners/big-logo/black/logo14.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo14.svg",subtitle:"Торговый партнер",name:"CommuniGate Systems",text:"Российский разработчик программного обеспечения в области интегрированных коммуникаций."},{imgWhite:"/images/partners/big-logo/white/logo15.svg",imgColor:"/images/partners/big-logo/color/logo15.svg",imgBlack:"/images/partners/big-logo/black/logo15.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo15.svg",subtitle:"Реселлер",name:"ROSA",text:"Российский разработчик системного ПО (настольные и серверные операционные системы, системы разворачивания облачных и инфраструктурных сервисов)."},{imgWhite:"/images/partners/big-logo/white/logo16.svg",imgColor:"/images/partners/big-logo/color/logo16.svg",imgBlack:"/images/partners/big-logo/black/logo16.svg",imgColorWhite:"/images/partners/big-logo/color-white/logo16.svg",subtitle:"Авторизованный партнер",name:"Yadro",text:"Российский производитель высокотехнологичного оборудования (серверы, системы хранения данных, инфраструктурные решения)."}],I=document.querySelector(".tab-partner");const N={TAB_ROW:".tabs",TAB_BUTTON:".tab-partners__item",TAB_CONTENT:".tab-partners__section",SECTION:".tab-partners",TAB_BTN_ACTIVE:".tab-partners__item_active",TAB_CONTENT_ACTIVE:".tab-partners__section_active"},q={TAB_BTN_ACTIVE:"tab-partners__item_active",TAB_CONTENT_ACTIVE:"tab-partners__section_active"};class M{constructor(o,e){this._selectors=o,this._classes=e}initTabs(){this._getElements(),this._setAttributes(),this._setListeners(),this._showByIndex(0)}_getElements(){this._section=document.querySelector(this._selectors.SECTION),this._tabRow=this._section.querySelector(this._selectors.TAB_ROW),this._headers=this._tabRow.querySelectorAll(this._selectors.TAB_BUTTON),this._contents=this._tabRow.querySelectorAll(this._selectors.TAB_CONTENT),this._activeTab=this._tabRow.querySelector(this._selectors.TAB_BTN_ACTIVE),this._activeTabContent=this._tabRow.querySelector(this._selectors.TAB_CONTENT_ACTIVE)}_setAttributes(){this._headers.forEach(((o,e)=>{o.dataset.index=e,o.setAttribute("role","tab"),this._contents[e].setAttribute("role","tab-content"),this._contents[e].dataset.index=e}))}_setListeners(){this._tabRow.addEventListener("click",(o=>{const e=o.target.closest(this._selectors.TAB_BUTTON);e&&(o.preventDefault(),this._setActiveTab(e))}))}_setActiveTab(o){o!==this._activeTab&&(this._toggleClass(!1),this._activeTab=o,this._activeTabContent=this._contents[o.dataset.index],this._toggleClass(!0))}setActiveTab(o){const e=this._headers[o];this._setActiveTab(e)}_toggleClass(o){this._activeTab&&this._activeTab.classList.toggle(this._classes.TAB_BTN_ACTIVE,o),this._activeTabContent&&this._activeTabContent.classList.toggle(this._classes.TAB_CONTENT_ACTIVE,o)}_showByIndex(o){this._setActiveTab(this._headers[o])}}!function(){try{const o=new M(N,q);o.initTabs();"1"===new URL(window.location.href).searchParams.get("tab")&&o.setActiveTab(1)}catch(o){}"dark"===localStorage.getItem("theme")?(f.forEach((o=>{o.checked=!1})),E()):(f.forEach((o=>{o.checked=!0})),y()),f.forEach((o=>{o.addEventListener("change",(()=>{o.checked?(f.forEach((o=>{o.checked=!0})),localStorage.setItem("theme","light"),y()):(f.forEach((o=>{o.checked=!1})),localStorage.setItem("theme","dark"),E())}))})),function(){try{const e=document.querySelector(".js_header_btn"),t=document.querySelector(".js_menu"),i=document.querySelector(".js_menu_overlay"),g=document.querySelector(".js_menu_exit");t.style.display="",e.addEventListener("click",(()=>{i.classList.add("active"),t.classList.add("active-half"),g.classList.add("active"),o.unscroll()})),g.addEventListener("click",(()=>{s()})),i.addEventListener("click",(()=>{s()}));const s=()=>{i.classList.remove("active"),t.classList.remove("active-half","active-full"),g.classList.remove("active"),o.scroll()}}catch(o){}}(),function(){try{const o=document.querySelector("#popupform"),e=new t(o,(()=>{e.closePopup()}));e.setEventListeners(),document.querySelectorAll(".popup-form-open").forEach((o=>{o.addEventListener("click",(o=>{const t=o.target.dataset.subject;e.openPopup(t)}))}))}catch(o){console.log(o)}}(),function(){A.forEach(((o,e)=>{const t=function(o,e){const t=x("tab-clients__img-box","logo-img-box"),i=B(o.imgWhite,"tab-clients__img-white","logo-img-box__grey-light"),g=B(o.imgBlack,"tab-clients__img-black","logo-img-box__grey-dark"),s=B(o.imgColor,"tab-clients__img-color","logo-img-box__color-light"),l=B(o.imgColorWhite,"tab-clients__img-color-white","logo-img-box__color-dark");return t.append(i),t.append(g),t.append(s),t.append(l),e||t.classList.add("logo_unvisible"),t}(o,e<8);S.append(t)}));const o=document.querySelector(".tab-clients__btn");o.addEventListener("click",(()=>{document.querySelectorAll(".logo_unvisible").forEach((e=>{e.classList.remove("logo_unvisible"),o.style.display="none"}))}))}(),L.forEach(((o,e)=>{if(e<8){const e=function(o,e){const t=x("tab-partner__img-box","logo-img-box"),i=B(o.imgWhite,"tab-partner__img-white","logo-img-box__grey-light"),g=B(o.imgBlack,"tab-partner__img-black","logo-img-box__grey-dark"),s=B(o.imgColor,"tab-partner__img-color","logo-img-box__color-light"),l=B(o.imgColorWhite,"tab-partner__img-color-white","logo-img-box__color-dark");return t.append(i),t.append(g),t.append(s),t.append(l),t}(o);I.append(e)}}))}()})();
(() => {
	"use strict";
	const o = {
		scroll() {
			document.body.style.overflow = "";
		},
		unscroll() {
			document.body.style.overflow = "hidden";
		},
	};
	class e {
		constructor(o) {
			(this._popup = o),
				(this.openPopup = this.openPopup.bind(this)),
				(this.closePopup = this.closePopup.bind(this)),
				(this._setEscHandler = this._setEscHandler.bind(this)),
				(this.setEventListeners = this.setEventListeners.bind(this));
		}
		openPopup(e) {
			e && (this._subject = e), this._popup.classList.add("popup-form_visible"), (document.body.style.overflow = "hidden"), o.unscroll(), document.addEventListener("keydown", this._setEscHandler);
		}
		closePopup() {
			this._popup.classList.remove("popup-form_visible"), o.scroll(), document.removeEventListener("keydown", this._setEscHandler);
		}
		_setEscHandler(o) {
			"Escape" === o.key && this.closePopup();
		}
		setEventListeners() {
			this._popup.addEventListener("mousedown", (o) => {
				(o.target.classList.contains("popup-form_visible") || o.target.closest(".popup-form__close")) && this.closePopup();
			});
		}
	}
	class t extends e {
		constructor(o, e) {
			super(o),
				(this._form = o.querySelector(".form")),
				(this._onSubmit = e),
				(this._inputList = this._form.querySelectorAll(".form__item")),
				(this._popupButton = this._form.querySelector(".form__btn")),
				(this._inputValues = {}),
				(this._hiddenInput = this._form.querySelector(".form__fields_input-hidden"));
		}
		openPopup(o) {
			super.openPopup(o), (this._hiddenInput.value = this._subject), console.log(this._hiddenInput.value);
		}
		closePopup() {
			this._form.reset(), super.closePopup();
		}
		_getInputValues() {
			return (
				this._inputList.forEach((o) => {
					this._inputValues[o.name] = o.value;
				}),
					this._inputValues
			);
		}
		setEventListeners() {
			super.setEventListeners(),
				this._form.addEventListener("submit", (o) => {
					o.preventDefault();
				});
		}
	}
	const i = { WRAPPER: document.querySelector(".wrapper"), CANVAS: document.querySelector(".canvas") },
		g = 0.003,
		s = 20,
		l = 0,
		a = 0,
		r = s,
		n = { MIN: -180, MAX: 180 },
		c = { MIN: -90, MAX: 90 },
		m = "#202020",
		h = "#FBFBFB",
		p = "#A42E2C",
		b = "#670E0E",
		_ = "#511112",
		u = "#340D0F",
		d = { rotation: 0, time: 0, ctx: void 0, dots: [], background: h, KKK: 0, WIDTH: 0, HEIGHT: 0, colorFill: p, colorStroke: b };
	try {
		T(),
			requestAnimationFrame(T),
			(function () {
				for (let o = 0; o < 500; o++) d.dots.push(W());
			})(),
			(function () {
				const o = i.CANVAS.getContext("2d");
				o.fillRect(0, 0, i.CANVAS.width, i.CANVAS.height), (d.ctx = o);
			})(),
			w(),
			w();
	} catch (o) {}
	function v(o) {
		o ? ((d.background = h), (d.colorFill = _), (d.colorStroke = u)) : ((d.background = m), (d.colorFill = p), (d.colorStroke = b));
	}
	function k(o) {
		const e = s / o.z,
			t = d.WIDTH / 2,
			i = d.HEIGHT / 2,
			g = d.KKK * e * o.x + t,
			l = -d.KKK * e * o.y + i;
		(o.xDisplay = g), (o.yDisplay = l), d.ctx.beginPath(), d.ctx.ellipse(g, l, 2, 2, 0, 0, 2 * Math.PI), d.ctx.fill();
	}
	function C(o) {
		o.links.forEach((e) => {
			d.ctx.beginPath(), d.ctx.moveTo(o.xDisplay, o.yDisplay), d.ctx.lineTo(e.xDisplay, e.yDisplay), d.ctx.stroke();
		});
	}
	function w() {
		T(),
			d.time++,
			d.dots.forEach((o) => {
				(o.a = o.a + o.da),
					(o.b = o.b + o.db),
					(function (o) {
						(o.x = l + 1 * Math.sin(-d.rotation - d.time / 1e3 + o.a) * Math.cos(o.b)), (o.y = a + 1 * Math.sin(o.b)), (o.z = r - 1 * Math.cos(o.b) * Math.cos(-d.rotation - d.time / 1e3 + o.a));
					})(o);
			}),
			d.dots.sort((o, e) => e.z - o.z),
			d.dots.forEach((o) => {
				(o.links = []),
					d.dots.forEach((e) => {
						var t, i;
						(t = o), (i = e), Math.sqrt(Math.pow(t.x - i.x, 2) + Math.pow(t.y - i.y, 2) + Math.pow(t.z - i.z, 2)) < 0.15 && o.links.push(e);
					});
			}),
			(d.ctx.fillStyle = d.background),
			d.ctx.fillRect(0, 0, d.WIDTH, d.HEIGHT),
			(d.ctx.fillStyle = d.colorFill),
			(d.ctx.strokeStyle = d.colorStroke),
			d.dots.forEach(C),
			d.dots.forEach(k),
			requestAnimationFrame(w);
	}
	function W(...o) {
		return o.length > 0
			? { a: o[0], b: o[1], da: 0, db: 0, xDisplay: 0, yDisplay: 0, links: [] }
			: { a: (n.MAX - n.MIN) * Math.random() + n.MIN, b: (c.MAX - c.MIN) * Math.random() + c.MIN, da: g * Math.random() - 0.0015, db: g * Math.random() - 0.0015, links: [] };
	}
	function T() {
		const o = i.CANVAS.clientWidth,
			e = i.CANVAS.clientHeight,
			t = Math.min(o, e);
		(d.KKK = t / 3), (d.WIDTH = o), (d.HEIGHT = e), (i.CANVAS.width = o), (i.CANVAS.height = e);
	}
	const f = document.querySelectorAll(".js_toggle_input");
	function y() {
		document.body.classList.add("light-mode"), v(!0), console.log("светлая тема");
	}
	function E() {
		document.body.classList.remove("light-mode"), v(!1), console.log("тёмная тема");
	}
	const A = [
	/*	{ imgWhite: "/images/clients/white/logo1.svg", imgColor: "/images/clients/color/logo1.svg", imgBlack: "/images/clients/black/logo1.svg", imgColorWhite: "/images/clients/color-white/logo1.svg" },
		{ imgWhite: "/images/clients/white/logo2.svg", imgColor: "/images/clients/color/logo2.svg", imgBlack: "/images/clients/black/logo2.svg", imgColorWhite: "/images/clients/color-white/logo2.svg" },
		{ imgWhite: "/images/clients/white/logo3.svg", imgColor: "/images/clients/color/logo3.svg", imgBlack: "/images/clients/black/logo3.svg", imgColorWhite: "/images/clients/color-white/logo3.svg" },
		{ imgWhite: "/images/clients/white/logo4.svg", imgColor: "/images/clients/color/logo4.svg", imgBlack: "/images/clients/black/logo4.svg", imgColorWhite: "/images/clients/color-white/logo4.svg" },
		{ imgWhite: "/images/clients/white/logo5.png", imgColor: "/images/clients/color/logo5.png", imgBlack: "/images/clients/black/logo5.png", imgColorWhite: "/images/clients/color-white/logo5.png" },
		{ imgWhite: "/images/clients/white/logo6.svg", imgColor: "/images/clients/color/logo6.svg", imgBlack: "/images/clients/black/logo6.svg", imgColorWhite: "/images/clients/color-white/logo6.svg" },
		{ imgWhite: "/images/clients/white/logo7.svg", imgColor: "/images/clients/color/logo7.svg", imgBlack: "/images/clients/black/logo7.svg", imgColorWhite: "/images/clients/color-white/logo7.svg" },
		{ imgWhite: "/images/clients/white/logo8.png", imgColor: "/images/clients/color/logo8.png", imgBlack: "/images/clients/black/logo8.png", imgColorWhite: "/images/clients/color-white/logo8.png" },
		{ imgWhite: "/images/clients/white/logo9.png", imgColor: "/images/clients/color/logo9.png", imgBlack: "/images/clients/black/logo9.png", imgColorWhite: "/images/clients/color-white/logo9.png" },
		{ imgWhite: "/images/clients/white/logo10.png", imgColor: "/images/clients/color/logo10.png", imgBlack: "/images/clients/black/logo10.png", imgColorWhite: "/images/clients/color-white/logo10.png" },
		{ imgWhite: "/images/clients/white/logo11.png", imgColor: "/images/clients/color/logo11.png", imgBlack: "/images/clients/black/logo11.png", imgColorWhite: "/images/clients/color-white/logo11.png" },
		{ imgWhite: "/images/clients/white/logo12.png", imgColor: "/images/clients/color/logo12.png", imgBlack: "/images/clients/black/logo12.png", imgColorWhite: "/images/clients/color-white/logo12.png" },
		{ imgWhite: "/images/clients/white/logo13.png", imgColor: "/images/clients/color/logo13.png", imgBlack: "/images/clients/black/logo13.png", imgColorWhite: "/images/clients/color-white/logo13.png" },
		{ imgWhite: "/images/clients/white/logo14.png", imgColor: "/images/clients/color/logo14.png", imgBlack: "/images/clients/black/logo14.png", imgColorWhite: "/images/clients/color-white/logo14.png" },
		{ imgWhite: "/images/clients/white/logo15.png", imgColor: "/images/clients/color/logo15.png", imgBlack: "/images/clients/black/logo15.png", imgColorWhite: "/images/clients/color-white/logo15.png" },
		{ imgWhite: "/images/clients/white/logo16.png", imgColor: "/images/clients/color/logo16.png", imgBlack: "/images/clients/black/logo16.png", imgColorWhite: "/images/clients/color-white/logo16.png" },
		{ imgWhite: "/images/clients/white/logo17.png", imgColor: "/images/clients/color/logo17.png", imgBlack: "/images/clients/black/logo17.png", imgColorWhite: "/images/clients/color-white/logo17.png" },*/
	];
	function B(o, ...e) {
		const t = document.createElement("img");
		return (t.src = o), t.classList.add(...e), t;
	}
	function x(...o) {
		const e = document.createElement("div");
		return e.classList.add(...o), e;
	}
	const S = document.querySelector(".tab-clients");
	// const L = [
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo1.png",
	// 			imgColor: "/images/partners/big-logo/color/logo1.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo1.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo1.png",
	// 			subtitle: "",
	// 			name: "Код безопасности",
	// 			text: "Российский разработчик программных и аппаратных средств, обеспечивающих полную защиту ИТ-инфраструктуры: конечных станций и серверов, периметра сети, современных виртуальных инфраструктур и мобильных устройств.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo2.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo2.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo2.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo2.svg",
	// 			subtitle: "Авторизованный партнер",
	// 			name: "Positive technologies",
	// 			text: "Российский разработчик решений для информационной безопасности.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo3.png",
	// 			imgColor: "/images/partners/big-logo/color/logo3.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo3.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo3.png",
	// 			subtitle: "Registered Partner",
	// 			name: "Лаборатория Касперского",
	// 			text: "Российский разработчик и производитель систем защиты от компьютерных вирусов, спама, хакерских атак и прочих киберугроз.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo4.png",
	// 			imgColor: "/images/partners/big-logo/color/logo4.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo4.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo4.png",
	// 			subtitle: "Partner",
	// 			name: "Check Point",
	// 			text: "Израильский разработчик решений по кибербезопасности.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo5.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo5.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo5.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo5.svg",
	// 			subtitle: "Bronze",
	// 			name: "UserGate",
	// 			text: "Российский разработчик программного обеспечения и микроэлектроники в сфере информационной безопасности.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo6.png",
	// 			imgColor: "/images/partners/big-logo/color/logo6.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo6.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo6.png",
	// 			subtitle: "",
	// 			name: "Ruseim",
	// 			text: "Российский производитель, занимающийся созданием решений в области мониторинга и управления событиями информационной безопасности на основе анализа данных в реальном времени.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo7.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo7.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo7.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo7.svg",
	// 			subtitle: "",
	// 			name: "Aquarius",
	// 			text: "Российский производитель компьютерной техники (серверы, системы хранения данных, клиентские устройства, отраслевые и специализированные IT‑решения и программно-аппаратные комплексы.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo8.png",
	// 			imgColor: "/images/partners/big-logo/color/logo8.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo8.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo8.png",
	// 			subtitle: "Бизнес-партнер",
	// 			name: "Utinet",
	// 			text: "Российский роизводитель серверов, систем хранения данных и программного обеспечения.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo9.png",
	// 			imgColor: "/images/partners/big-logo/color/logo9.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo9.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo9.png",
	// 			subtitle: "Авторизованный партнер",
	// 			name: "Аэродиск",
	// 			text: "Российский производитель инновационных решений в области хранения данных и виртуализации (системы хранения данных, гиперконвергентная система и программное обеспечение).",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo10.png",
	// 			imgColor: "/images/partners/big-logo/color/logo10.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo10.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo10.png",
	// 			subtitle: "Авторизованный партнер",
	// 			name: "Звезда",
	// 			text:
	// 				"Российский разработчик и производитель вычислительного радиоэлектронного оборудования и ПО (вычислительные серверы, программно-определяемые системы хранения данных, автоматизированные рабочие места и тонкие клиенты). телекоммуникационное оборудование)",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo11.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo11.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo11.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo11.svg",
	// 			subtitle: "",
	// 			name: "Eltex",
	// 			text: "Российский разработчик и производитель телекоммуникационного и сетевого оборудования, промышленных контроллеров для IoT.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo12.png",
	// 			imgColor: "/images/partners/big-logo/color/logo12.png",
	// 			imgBlack: "/images/partners/big-logo/black/logo12.png",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo12.png",
	// 			subtitle: "Авторизованный партнер",
	// 			name: "Р7-Офис",
	// 			text: "Российский разработчик офисного программного обеспечения.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo13.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo13.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo13.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo13.svg",
	// 			subtitle: "Авторизованный партнер",
	// 			name: "Мой офис",
	// 			text: "Российский разработчик безопасных офисных решений для общения и совместной работы с документами.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo14.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo14.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo14.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo14.svg",
	// 			subtitle: "Торговый партнер",
	// 			name: "CommuniGate Systems",
	// 			text: "Российский разработчик программного обеспечения в области интегрированных коммуникаций.",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo15.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo15.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo15.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo15.svg",
	// 			subtitle: "Реселлер",
	// 			name: "ROSA",
	// 			text: "Российский разработчик системного ПО (настольные и серверные операционные системы, системы разворачивания облачных и инфраструктурных сервисов).",
	// 		},
	// 		{
	// 			imgWhite: "/images/partners/big-logo/white/logo16.svg",
	// 			imgColor: "/images/partners/big-logo/color/logo16.svg",
	// 			imgBlack: "/images/partners/big-logo/black/logo16.svg",
	// 			imgColorWhite: "/images/partners/big-logo/color-white/logo16.svg",
	// 			subtitle: "Авторизованный партнер",
	// 			name: "Yadro",
	// 			text: "Российский производитель высокотехнологичного оборудования (серверы, системы хранения данных, инфраструктурные решения).",
	// 		},
	// 	],
	const L = [],
		I = document.querySelector(".tab-partner");
	const N = { TAB_ROW: ".tabs", TAB_BUTTON: ".tab-partners__item", TAB_CONTENT: ".tab-partners__section", SECTION: ".tab-partners", TAB_BTN_ACTIVE: ".tab-partners__item_active", TAB_CONTENT_ACTIVE: ".tab-partners__section_active" },
		q = { TAB_BTN_ACTIVE: "tab-partners__item_active", TAB_CONTENT_ACTIVE: "tab-partners__section_active" };
	class M {
		constructor(o, e) {
			(this._selectors = o), (this._classes = e);
		}
		initTabs() {
			this._getElements(), this._setAttributes(), this._setListeners(), this._showByIndex(0);
		}
		_getElements() {
			(this._section = document.querySelector(this._selectors.SECTION)),
				(this._tabRow = this._section.querySelector(this._selectors.TAB_ROW)),
				(this._headers = this._tabRow.querySelectorAll(this._selectors.TAB_BUTTON)),
				(this._contents = this._tabRow.querySelectorAll(this._selectors.TAB_CONTENT)),
				(this._activeTab = this._tabRow.querySelector(this._selectors.TAB_BTN_ACTIVE)),
				(this._activeTabContent = this._tabRow.querySelector(this._selectors.TAB_CONTENT_ACTIVE));
		}
		_setAttributes() {
			this._headers.forEach((o, e) => {
				(o.dataset.index = e), o.setAttribute("role", "tab"), this._contents[e].setAttribute("role", "tab-content"), (this._contents[e].dataset.index = e);
			});
		}
		_setListeners() {
			this._tabRow.addEventListener("click", (o) => {
				const e = o.target.closest(this._selectors.TAB_BUTTON);
				e && (o.preventDefault(), this._setActiveTab(e));
			});
		}
		_setActiveTab(o) {
			o !== this._activeTab && (this._toggleClass(!1), (this._activeTab = o), (this._activeTabContent = this._contents[o.dataset.index]), this._toggleClass(!0));
		}
		setActiveTab(o) {
			const e = this._headers[o];
			this._setActiveTab(e);
		}
		_toggleClass(o) {
			this._activeTab && this._activeTab.classList.toggle(this._classes.TAB_BTN_ACTIVE, o), this._activeTabContent && this._activeTabContent.classList.toggle(this._classes.TAB_CONTENT_ACTIVE, o);
		}
		_showByIndex(o) {
			this._setActiveTab(this._headers[o]);
		}
	}
	!(function () {
		try {
			const o = new M(N, q);
			o.initTabs();
			"1" === new URL(window.location.href).searchParams.get("tab") && o.setActiveTab(1);
		} catch (o) {}
		"dark" === localStorage.getItem("theme")
			? (f.forEach((o) => {
				o.checked = !1;
			}),
				E())
			: (f.forEach((o) => {
				o.checked = !0;
			}),
				y()),
			f.forEach((o) => {
				o.addEventListener("change", () => {
					o.checked
						? (f.forEach((o) => {
							o.checked = !0;
						}),
							localStorage.setItem("theme", "light"),
							y())
						: (f.forEach((o) => {
							o.checked = !1;
						}),
							localStorage.setItem("theme", "dark"),
							E());
				});
			}),
			(function () {
				try {
					const e = document.querySelector(".js_header_btn"),
						t = document.querySelector(".js_menu"),
						i = document.querySelector(".js_menu_overlay"),
						g = document.querySelector(".js_menu_exit");
					(t.style.display = ""),
						e.addEventListener("click", () => {
							i.classList.add("active"), t.classList.add("active-half"), g.classList.add("active"), o.unscroll();
						}),
						g.addEventListener("click", () => {
							s();
						}),
						i.addEventListener("click", () => {
							s();
						});
					const s = () => {
						i.classList.remove("active"), t.classList.remove("active-half", "active-full"), g.classList.remove("active"), o.scroll();
					};
				} catch (o) {}
			})(),
			(function () {
				try {
					const o = document.querySelector("#popupform"),
						e = new t(o, () => {
							e.closePopup();
						});
					e.setEventListeners(),
						document.querySelectorAll(".popup-form-open").forEach((o) => {
							o.addEventListener("click", (o) => {
								const t = o.target.dataset.subject;
								e.openPopup(t);
							});
						});
				} catch (o) {
					console.log(o);
				}
			})(),
			(function () {
				A.forEach((o, e) => {
					const t = (function (o, e) {
						const t = x("tab-clients__img-box", "logo-img-box"),
							i = B(o.imgWhite, "tab-clients__img-white", "logo-img-box__grey-light"),
							g = B(o.imgBlack, "tab-clients__img-black", "logo-img-box__grey-dark"),
							s = B(o.imgColor, "tab-clients__img-color", "logo-img-box__color-light"),
							l = B(o.imgColorWhite, "tab-clients__img-color-white", "logo-img-box__color-dark");
						return t.append(i), t.append(g), t.append(s), t.append(l), e || t.classList.add("logo_unvisible"), t;
					})(o, e < 8);
					S.append(t);
				});
				const o = document.querySelector(".tab-clients__btn");
				o.addEventListener("click", () => {
					document.querySelectorAll(".logo_unvisible").forEach((e) => {
						e.classList.remove("logo_unvisible"), (o.style.display = "none");
					});
				});
			})(),
			L.forEach((o, e) => {
				if (e < 8) {
					const e = (function (o, e) {
						const t = x("tab-partner__img-box", "logo-img-box"),
							i = B(o.imgWhite, "tab-partner__img-white", "logo-img-box__grey-light"),
							g = B(o.imgBlack, "tab-partner__img-black", "logo-img-box__grey-dark"),
							s = B(o.imgColor, "tab-partner__img-color", "logo-img-box__color-light"),
							l = B(o.imgColorWhite, "tab-partner__img-color-white", "logo-img-box__color-dark");
						return t.append(i), t.append(g), t.append(s), t.append(l), t;
					})(o);
					I.append(e);
				}
			});
	})();
})();
