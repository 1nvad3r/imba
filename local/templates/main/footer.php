<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	$curPage = $APPLICATION->GetCurPage(false);
?>
<?php if ($curPage == '/partners/'): ?>
	<section class="popup-logo">
		<div class="popup-logo__container">
			<button class="popup-logo__close popup__btn-close">
				<svg width="16" height="16">
					<use xlink:href="/images/sprite.svg#exit"></use>
				</svg>
			</button>
			<div class="popup-logo__content">
				<div class="popup-logo__img-box"><img class="popup-logo__img"
													  src="/images/partners/big-logo/color/logo2.svg" alt=""/>
					<div class="popup-logo__tag">
						<div class="popup-logo__tag-marker"></div>
						<p class="text-s popup-logo__tag-text">Бизнес-партнер</p></div>
				</div>
				<div class="popup-logo__info"><p class="lead-title popup-logo__name">Utinet</p>
					<p class="lead-text popup-logo__text">Российский роизводитель серверов, систем хранения данных и
						программного обеспечения.</p></div>
			</div>
		</div>
	</section>
<?php endif;?>
<?php if ($curPage == '/contacts/'): ?>
	</div>
	<div class="cta-contacts__column cta-contacts__column-form"><p
				class="lead-text cta-contacts__form-title fade-in">Форма обратной связи</p>
		<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/forma.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false, ['HIDE_ICONS' => 'Y']); ?>
	</div>
	</div>
	</section>
	<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/map.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false, ['HIDE_ICONS' => 'Y']); ?>
	<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/requisites.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false, ['HIDE_ICONS' => 'Y']); ?>
<? endif; ?>
<section class="popup-form" id="popupform">
	<div class="popup-form__container">
		<button class="popup-form__close popup__btn-close" type="button">
			<svg width="16" height="16">
				<use xlink:href="/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<div class="popup-form__content"><p class="lead-text popup-form__title">Оставите заявку на БЕСПЛАТНУЮ
				КОНСУЛЬТАЦИЮ</p>
			<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/include/forma.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false, ['HIDE_ICONS' => 'Y']); ?>
		</div>
	</div>
</section>
<section class="feedback js_feedback_success">
	<div class="feedback__wrap">
		<button class="feedback__exit js_feedback_exit popup-form__close" type="button">
			<svg width="16" height="16">
				<use xlink:href="/images/sprite.svg#exit"></use>
			</svg>
		</button>
		<div class="feedback__success"><h3 class="feedback__success-subtitle">Спасибо</h3>
			<h2 class="feedback__success-title">Благодарим за&nbsp;заявку</h2>
			<p class="feedback__success-text">Наши менеджеры свяжутся с&nbsp;вами в&nbsp;рабочее время с&nbsp;10&nbsp;до&nbsp;18&nbsp;по&nbsp;Москве.
				Будьте на&nbsp;связи.</p></div>
	</div>
</section>
<div id="captcha-container"></div>
<footer class="section footer">
	<div class="container footer__container fade-in">
		<div class="footer__box">
			<div class="footer__content">
				<div class="footer__content-cta"><a class="footer__logo" href="/"><img src="/images/logo.svg"
																					   alt="Лого">
						<img src="/images/logo-for-light-mode.svg" alt="Лого"></a>
					<p class="lead-text footer__title">Оставите заявку на БЕСПЛАТНУЮ КОНСУЛЬТАЦИЮ</p>
					<button class="button button_style_red footer__button popup-form-open" type="button"
							data-subject="Заявка с подвала">оставить заявку
					</button>
				</div>
				<div class="footer-menu">
					<?php $APPLICATION->IncludeComponent("bitrix:menu", "bottom", [
							"ROOT_MENU_TYPE" => "bottom",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "bottom",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "360000",
							"MENU_CACHE_USE_GROUPS" => "Y",
						]
					); ?>
				</div>
			</div>
			<div class="footer__contacts">
				<span class="h3 footer-menu__contact-link">
				<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/phone.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?>
					</span>
				<span class="h3 footer-menu__contact-link">
				<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/email.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?>
					</span>
				<div class="text footer-menu__contact-address">
					<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_address.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?>
				</div>
				<a class="footer-menu__contact-social" href="https://t.me/imbait" target="_blank"> <img
							class="footer-menu__contact-icon" src="/images/Telegram.svg" alt="Телеграм"><img
							class="footer-menu__contact-icon footer-menu__contact-icon_black"
							src="/images/Telegram-black.svg" alt="Телеграм"></a></div>
		</div>
		<div class="footer__copy">
			<div class="footer__copy-line"></div>
			<div class="footer__copy-box">
				<div class="text-s footer__copy-text">
					&copy;&nbsp;<?= date('Y'); ?> <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_copyright.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?></div>
				<a class="text-s footer__link footer__link_type_developer" href="https://nologostudio.ru" target="_blank">Сайт
					разработан: <span>No Logo Studio</span></a></div>
		</div>
	</div>
</footer>
</body>
</html>