<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
CJSCore::Init(array("jquery"));
$curPage = $APPLICATION->GetCurPage(false);
$assets = \Bitrix\Main\Page\Asset::getInstance();

switch ($curPage) :
	case '/contacts/':
		$css = 'contacts.css';
		$js = 'contacts.js';
		break;
	case '/partners/':
		$css = 'partners.css';
		$js = 'partners.js';
		break;
	case '/requisites/':
		$css = 'requisites.css';
		$js = 'requisites.js';
		break;
	case '/services/':
		$css = 'ib-imba.css';
		$js = 'ibImba.js';
		break;
	case '/':
	case '/about/':
		$css = 'index.css';
		$js = 'index.js';
endswitch;
?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php $APPLICATION->ShowTitle() ?></title>
	<?php $APPLICATION->ShowHead() ?>
	<meta property="og:type" content="website">
	<meta property="og:title"
		  content="<?php $APPLICATION->ShowTitle();?>">
	<meta property="og:description"
		  content="<?php $APPLICATION->ShowProperty('description');?>">
	<meta property="og:url" content="https://imba-it.ru<?= $curPage ?>">
	<meta property="og:image" content="<?php $APPLICATION->ShowProperty('image');?>">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="630">
	<?php $assets->addCss(SITE_TEMPLATE_PATH . '/css/' . $css); ?>
	<?php $assets->addJs(SITE_TEMPLATE_PATH . '/js/commonScripts.js'); ?>
	<?php $assets->addJs(SITE_TEMPLATE_PATH . '/js/' . $js); ?>
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru" type="text/javascript" data-skip-moving="true"></script>
	<?php $APPLICATION->IncludeComponent("bitrix:main.include",	"",	["AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/include/metrika.php"], false); ?>
</head>
<body class="light-mode">
<?php $APPLICATION->ShowPanel() ?>
<body class="light-mode">
<div class="sub-header"></div>
<header class="header">
	<div class="container header__container">
		<div class="header__inner">
			<a class="header__logo" href="/" title="">
				<img src="/images/logo.svg" alt="ИМБА ИТ Лого">
				<img src="/images/logo-for-light-mode.svg" alt="ИМБА ИТ Лого">
			</a>
			<?php $APPLICATION->IncludeComponent("bitrix:menu", "top", [
					"ROOT_MENU_TYPE" => "top",
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "top",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_TIME" => "360000",
					"MENU_CACHE_USE_GROUPS" => "Y",
				]
			); ?>
			<div class="header__control">
				<button class="button button_style_red header-menu__button popup-form-open" type="button"
						data-subject="Заявка на обратный звонок c хэдера">Связаться с нами
				</button>
				<button class="header__btn js_header_btn">
					<svg width="66" height="26">
						<use xlink:href="/images/sprite.svg#burger"></use>
					</svg>
				</button>
				<label class="header__toggle toggle"><input id='toggleInput' class="js_toggle_input"
															type="checkbox"><span></span></label></div>
		</div>
	</div>
	<div class="burger__overlay js_menu_overlay"></div>
	<div class="burger js_menu" style="display:none;">
		<div class="burger__container">
			<button class="burger__exit js_menu_exit" type="button">
				<svg width="16" height="16">
					<use xlink:href="/images/sprite.svg#exit"></use>
				</svg>
			</button>
			<div class="burger__col">
				<?php $APPLICATION->IncludeComponent("bitrix:menu", "burger", [
						"ROOT_MENU_TYPE" => "left",
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "left",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_TIME" => "360000",
						"MENU_CACHE_USE_GROUPS" => "Y",
					]
				); ?>
				<div class="burger__contacts">
					<span class="h3 burger__contacts__link">
						<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/phone.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false); ?>
					</span>
					<span class="h3 burger__contacts__link">
						<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/email.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false); ?>
					</span>
				</div>
				<button class="button button_style_red burger__button popup-form-open" type="button"
						data-subject="Заявка на обратный звонок c хэдера">Связаться
				</button>
				<label class="burger__toggle toggle"><input id="toggleInputBurger" class="js_toggle_input"
															type="checkbox"><span></span></label></div>
		</div>
	</div>
</header>
<?php if ($curPage == '/contacts/'): ?>
<section class="section cover-contact">
	<div class="container cover-contact__container"><h1 class="h1 cover-contact__title fade-in">Контакты</h1>
		<div class="red-line cover-contact__lines fade-in"></div>
	</div>
</section>
<section class="section cta-contacts" id="contact-form">
	<div class="container cta-contacts__container">
		<div class="cta-contacts__column cta-contacts__column-info">
			<? endif; ?>
