<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetPageProperty("title", "Реквизиты компании и банковского счёта ИМБА ИТ / IMBA IT");
	$APPLICATION->SetTitle("Реквизиты");
?>
	<section class="section cover-requisites">
		<div class="container cover-requisites__container"><h1 class="h1 cover-requisites__title">Реквизиты</h1>
			<div class="red-line cover-requisites__lines"></div>
		</div>
	</section>
	<section class="section table-section">
		<div class="container table-section__container">
			<table class="table-requisites">
				<thead>
				<tr>
					<td class="lead-text table-requisites__title" colspan="2">ОБЩАЯ ИНФОРМАЦИЯ</td>
				</tr>
				</thead>
				<tbody>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Торговое наименование</td>
					<td class="text table-requisites__col-description">ИМБА ИТ</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Краткое наименование</td>
					<td class="text table-requisites__col-description">ООО «Инжиниринг Групп»</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Юридический адрес</td>
					<td class="text table-requisites__col-description">117246, г. Москва, Научный проезд, д. 14А, стр.
						1, этаж 4, пом. V
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Фактический адрес</td>
					<td class="text table-requisites__col-description">117246, г. Москва, Научный проезд, д. 14А, стр.
						1, этаж 4, пом. 4.3
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Генеральный директор</td>
					<td class="text table-requisites__col-description">Лыкин Антон Семенович</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Главный бухгалтер</td>
					<td class="text table-requisites__col-description">Кудрицкая Татьяна Викторовна</td>
				</tr>
				</tbody>
			</table>
			<table class="table-requisites">
				<thead>
				<tr>
					<td class="lead-text table-requisites__title" colspan="2">ЕГРЮЛ</td>
				</tr>
				</thead>
				<tbody>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ИНН</td>
					<td class="text table-requisites__col-description">7736131037</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">КПП</td>
					<td class="text table-requisites__col-description">772801001</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОГРН</td>
					<td class="text table-requisites__col-description">1157746278667</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Уставный капитал</td>
					<td class="text table-requisites__col-description">10000000 ₽</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Наименование регистрирующего органа</td>
					<td class="text table-requisites__col-description">Межрайонная инспекция Федеральной налоговой
						службы № 46 по г. Москве
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Дата регистрации</td>
					<td class="text table-requisites__col-description">26.03.2015</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Наименование налогового органа</td>
					<td class="text table-requisites__col-description">Инспекция Федеральной налоговой службы № 28 по
						г.Москве
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Дата постановки на учёт</td>
					<td class="text table-requisites__col-description">05.07.2018</td>
				</tr>
				</tbody>
			</table>
			<table class="table-requisites">
				<thead>
				<tr>
					<td class="lead-text table-requisites__title" colspan="2">КОДЫ СТАТИСТИКИ</td>
				</tr>
				</thead>
				<tbody>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКВЭД (основной вид деятельности)</td>
					<td class="text table-requisites__col-description">62.09 Деятельность, связанная с использованием
						вычислительной техники и информационных технологий, прочая
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКВЭД (дополнительные виды деятельности)</td>
					<td class="text table-requisites__col-description">33.13, 33.14, 33.20, 43.21, 43.29, 43.31, 43.32,
						43.33, 43.34, 47.63.2, 47.91.2, 47.91.3, 47.91.4, 61.10.9, 62.01, 62.02, 63.11, 63.11.1, 71.11,
						71.12, 71.12.1, 71.12.12, 71.12.13, 71.12.5, 71.12.6, 72.19, 77.33, 95.11
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКПО / Идентификационный номер ТОСП</td>
					<td class="text table-requisites__col-description">17196987</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКАТО</td>
					<td class="text table-requisites__col-description">45293590000</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКТМО</td>
					<td class="text table-requisites__col-description">45908000000</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКОГУ</td>
					<td class="text table-requisites__col-description">4210014</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКФС</td>
					<td class="text table-requisites__col-description">16</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">ОКОПФ</td>
					<td class="text table-requisites__col-description">12300</td>
				</tr>
				</tbody>
			</table>
			<table class="table-requisites">
				<thead>
				<tr>
					<td class="lead-text table-requisites__title" colspan="2">СЧЁТ</td>
				</tr>
				</thead>
				<tbody>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Расчётный счёт</td>
					<td class="text table-requisites__col-description">40702810600000162936</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Корреспондентский счёт</td>
					<td class="text table-requisites__col-description">30101810200000000700</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Банк</td>
					<td class="text table-requisites__col-description">АО «Райффайзенбанк»</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">Адрес банка</td>
					<td class="text table-requisites__col-description">129090, Москва, ул.Троицкая, д.17, стр.&nbsp;1
					</td>
				</tr>
				<tr class="table-requisites__row">
					<td class="fact table-requisites__col-name">БИК</td>
					<td class="text table-requisites__col-description">044525700</td>
				</tr>
				</tbody>
			</table>
			<a class="button button_style_outlined table-requisites__button" href="/files/rekvizity-IMBA-IT.xlsx"
			   target="_blank"> Скачать реквизиты</a></div>
	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>