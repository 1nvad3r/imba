<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetPageProperty("title", "О компании ИМБА ИТ");
	$APPLICATION->SetTitle("О компании");
?>
	<div class="wrapper">
		<section class="cover-main">
			<div class="container cover-main__container">
				<h1 class="h1 cover-main__title">
					<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/about_title.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text"], false); ?>
				</h1>
			</div>
		</section>
		<section class="section services">
			<div class="container services__container">
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "services", [
					"IBLOCK_TYPE" => "news",
					"IBLOCK_ID" => IBID_SERVICES,
					"SET_TITLE" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_SHADOW" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"FIELD_CODE" => ["DETAIL_PICTURE"],
					"PROPERTY_CODE" => ["LIST_IMAGE"]
				],
					false
				); ?>
			</div>
		</section>
	</div>
	<section class="section about" id="about">
		<div class="container about__container">
			<h2 class="h2 about__title fade-in">
				<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/about_title.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?>
			</h2>
			<div class="about-info">
				<div class="about-info__text">
					<div class="red-line about-info__lines fade-in"></div>
					<p class="text about-info__description fade-in">
						<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/about_text.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?>
					</p>
				</div>
				<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/about_list.php", "AREA_FILE_RECURSIVE" => "N", "EDIT_MODE" => "text",], false); ?>
			</div>
		</div>
	</section>
	<section class="section licenses-partners">
		<div class="container licenses-partners__container"><h2 class="h2 licenses-partners__title">Лицензии</h2>
			<? $APPLICATION->IncludeComponent("bitrix:news.list", "licenses", [
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => IBID_LICENSES,
				"SET_TITLE" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_SHADOW" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
			],
				false
			); ?>
		</div>
	</section>
	<?php $APPLICATION->IncludeComponent("bitrix:main.include",	"",	["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/partner_tabs.php"], false, ['HIDE_ICONS' => 'Y']); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>