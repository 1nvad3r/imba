<?php
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("iblock");
	header('Content-type: application/json');

	if($_REQUEST['send_form'] == 'Y') {

		$el = new CIBlockElement;
		$PROP = [];
		$PROP[16] = strip_tags($_REQUEST['form_text_2']);
		$PROP[17] = strip_tags($_REQUEST['form_text_3']);
		$PROP[18] = strip_tags($_REQUEST['form_text_4']);

		$name = 'Сообщение от ' . strip_tags($_REQUEST['form_text_1']) . ' за ' . date('d.m.Y H:i:s');

		$arLoadProductArray = [
			'IBLOCK_ID' => IBID_FEEDBACK,
			'PROPERTY_VALUES' => $PROP,
			'NAME' => $name,
			'PREVIEW_TEXT' => strip_tags($_REQUEST['form_textarea_5']),
			'ACTIVE' => 'Y',
		];

		if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
			$to_send = [];
			$to_send['AUTHOR_EMAIL'] = strip_tags($_REQUEST['form_text_3']);
			$to_send['AUTHOR'] = strip_tags($_REQUEST['form_text_1']);
			$to_send['PHONE'] = strip_tags($_REQUEST['form_text_2']);
			$to_send['COMPANY'] = strip_tags($_REQUEST['form_text_4']);
			$to_send['TEXT'] = strip_tags($_REQUEST['form_textarea_5']);

			CEvent::SendImmediate ('FEEDBACK_FORM', SITE_ID, $to_send);
		} else {
			echo 'Error: ' . $el->LAST_ERROR;
		}
	}
