<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetPageProperty("title", "Крупные российкие и зарубежные партнёры системного интегратора ИМБА ИТ");
	$APPLICATION->SetTitle("Партнеры");
	global $arrFilter;
?>
	<section class="section cover-partners">
		<div class="container cover-partners__container"><h1 class="h1 cover-partners__title">Партнеры</h1>
			<div class="red-line cover-partners__lines"></div>
		</div>
	</section>
	<section class="section licenses-partners">
		<div class="container licenses-partners__container"><h2 class="h2 licenses-partners__title">Лицензии</h2>
			<? $APPLICATION->IncludeComponent("bitrix:news.list", "licenses", [
				"IBLOCK_TYPE" => "contacts",
				"IBLOCK_ID" => IBID_LICENSES,
				"SET_TITLE" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_SHADOW" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
			],
				false
			); ?>
		</div>
	</section>

<? $APPLICATION->IncludeComponent("bitrix:news.list", "big-partners", [
	"IBLOCK_TYPE" => "content",
	"IBLOCK_ID" => IBID_PARTNERS,
	"SET_TITLE" => "N",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_SHADOW" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => ["DETAIL_PICTURE", "TAGS"],
	"PROPERTY_CODE" => ["BASIC_PARTNER"],
],
	false
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>