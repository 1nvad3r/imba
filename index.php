<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetPageProperty("keywords", "IT-решения, интегратор, автоматизация, сопровождение");
	$APPLICATION->SetPageProperty("description", "Внедряем технологии в сфере информационной безопасности → Защищаем ИТ-инфракструтуру → Заменяем и обслуживаем импортное оборудование и ПО → Повышение производительности ЦОД → Диагностика, регламентные работы, ремонт, настройка инженерных систем");
	$APPLICATION->SetPageProperty("title", "Российский системный интегратор полного цикла &mdash; повышаем эффективность, автоматизируем процессы, внедряем продвинутые технологии");
	$APPLICATION->SetTitle("Главная страница");
	global $arrFilter;
?>
	<div class="wrapper">
	<section class="cover-main">
		<div class="container cover-main__container">
			<h1 class="h1 cover-main__title">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					[
						"AREA_FILE_RECURSIVE" => "N",
						"AREA_FILE_SHOW" => "file",
						"EDIT_MODE" => "text",
						"PATH" => SITE_DIR . "include/about_title.php",
					]
				); ?> </h1>
		</div>
	</section>
	<section class="section services">
		<div class="container services__container">
			<? $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"services",
				[
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_SHADOW" => "Y",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"FIELD_CODE" => ["DETAIL_PICTURE"],
					"IBLOCK_ID" => IBID_SERVICES,
					"IBLOCK_TYPE" => "news",
					"PROPERTY_CODE" => ["LIST_IMAGE"],
					"SET_TITLE" => "N",
				]
			); ?>
		</div>
	</section>
	</div>
	<section class="section about" id="about">
		<div class="container about__container">
			<h2 class="h2 about__title fade-in">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					[
						"AREA_FILE_RECURSIVE" => "N",
						"AREA_FILE_SHOW" => "file",
						"EDIT_MODE" => "text",
						"PATH" => SITE_DIR . "include/about_title.php",
					]
				); ?> </h2>
			<div class="about-info">
				<div class="about-info__text">
					<div class="red-line about-info__lines fade-in">
					</div>
					<p class="text about-info__description fade-in">
						<? $APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							[
								"AREA_FILE_RECURSIVE" => "N",
								"AREA_FILE_SHOW" => "file",
								"EDIT_MODE" => "text",
								"PATH" => SITE_DIR . "include/about_text.php",
							]
						); ?>
					</p>
				</div>
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					[
						"AREA_FILE_RECURSIVE" => "N",
						"AREA_FILE_SHOW" => "file",
						"EDIT_MODE" => "text",
						"PATH" => SITE_DIR . "include/about_list.php",
					]
				); ?>
			</div>
		</div>
	</section>
	<section class="section licenses-partners">
		<div class="container licenses-partners__container">
			<h2 class="h2 licenses-partners__title">Лицензии</h2>
			<? $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"licenses",
				[
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_SHADOW" => "Y",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"IBLOCK_ID" => IBID_LICENSES,
					"IBLOCK_TYPE" => "content",
					"SET_TITLE" => "N",
				]
			); ?>
		</div>
	</section>
<?php $APPLICATION->IncludeComponent("bitrix:main.include",	"",	["AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/partner_tabs.php"], false, ['HIDE_ICONS' => 'Y']); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>