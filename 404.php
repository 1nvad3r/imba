<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$assets = \Bitrix\Main\Page\Asset::getInstance();
$assets->addCss(SITE_TEMPLATE_PATH . '/css/404.css');

	$APPLICATION->SetTitle("404 Страница не найдена");?>
	<section class="section page-404">
		<div class="container page-404__container">
			<div class="page-404__wrap">
				<div class="page-404__inner">
					<div class="page-404__img"><img src="images/404.svg" alt="404"/><img src="/images/404-light.svg"
																						 alt="404"/><span></span></div>
					<h3 class="h3 page-404__subtitle">404</h3>
					<h2 class="h2 page-404__title">Видимо вы&nbsp;заблудились</h2>
					<p class="page-404__text">Вы&nbsp;оказались на&nbsp;странице, которая или удалена, или перемещена, или
						не&nbsp;существует. Попробуйте вернуться на&nbsp;главную.</p><a
						class="button button_style_red page-404__btn" href="/" title="">вернуться на&nbsp;главную</a></div>
			</div>
		</div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>